#! /ussr/bin/env python
# -*- coding: utf-8 -*-

import vtkio
import numpy as np

import plothelpers
from matplotlib import pyplot

if __name__ == '__main__':
    # read the file
    psi, bounds = vtkio.reader( 'data/mario1.vti' )

    vtkio.writer( 'data/mario2.vti', psi, bounds )

    psi2 = np.rot90( psi )
    vtkio.writer( 'data/mario3.vti', psi2, bounds )

    psi3 = np.fliplr( psi )
    vtkio.writer( 'data/mario4.vti', psi3, bounds )

    psi4 = np.flipud( psi )
    vtkio.writer( 'data/mario5.vti', psi4, bounds )