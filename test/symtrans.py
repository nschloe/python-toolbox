#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Provides symmetry transformations on the square.

"""
import sys
#from optparse import OptionParser # handle command line arguments
import vtkio
import numpy as np
from math import sqrt

# ==============================================================================
def rot90( psi )
    return np.rot90(psi)
# ==============================================================================
def rot180( psi ):
    return np.rot90( np.rot90(psi) )
# ==============================================================================
def conjugated_fliplr( psi  ):
    return np.fliplr(psi).conjugate()
# ==============================================================================
def fliplr( psi ):
    return np.fliplr(psi)
# ==============================================================================
def conjugated_flipud( psi  ):
    return flipud(psi).conjugate()
# ==============================================================================
def flipud( psi, h ):
    return np.flipud(psi)
# ==============================================================================
def diagonal1( psi, h ):
    return np.rot90( fliplr(psi).conjugate() )
# ==============================================================================
def check_diagonal2( psi ):
    return np.rot90( flipud(psi).conjugate() )
# ==============================================================================
def _main():
    """
    Main function.
    """

    infile = '../data/test/mario1.vti'
    outfile = 'test.vti'

    psi, bounds = vtkio.reader( infile )

    phi = conjugated_flipud( psi  )

    vtkio.writer( 'data/mario2.vti', phi, bounds )

    return 0
# ==============================================================================
if __name__ == '__main__':
    sys.exit( _main() )
# ==============================================================================