# ==============================================================================
import numpy as np
import scipy
import colorsys
# ==============================================================================
class NlsModelEvaluator:
    '''Simple model evalator for f(x) = (1-|z|^2)*z, split
    up in real an imaginary part.'''
    def __init__(self):
        self.dtype = float
    def compute_f(self, x):
        return np.array([x[0] * (1.0 - x[0]**2 - x[1]**2),
                         x[1] * (1.0 - x[0]**2 - x[1]**2)])
    def get_jacobian(self, x):
        return np.array([[(1.0 - x[0]**2 - x[1]**2) - 2.0*x[0]**2, -2.0*x[0]*x[1]],
                         [-2.0*x[0]*x[1], (1.0 - x[0]**2 - x[1]**2) - 2.0*x[1]**2]])
    def get_preconditioner(self, x):
        return None
    def get_preconditioner_inverse(self, x):
        return None
    def inner_product(self, x, y):
        return np.dot(x.T.conj(), y)
# ==============================================================================
class CubicModelEvaluator:
    '''Simple model evalator for f(x) = z^3 - 1, split
    up in real an imaginary part.'''
    def __init__(self):
        self.dtype = float
    def compute_f(self, x):
        return np.array([x[0]**3 - 3*x[0]*x[1]**2 - 1,
                         3*x[0]**2*x[1] - x[1]**3])
    def get_jacobian(self, x):
        return np.array([[3*x[0][0]**2 - 3*x[1][0]**2, - 6*x[0][0]*x[1][0]],
                         [6*x[0][0]*x[1][0], 3*x[0][0]**2 - 3*x[1][0]**2]])
    def get_preconditioner(self, x):
        return None
    def get_preconditioner_inverse(self, x):
        return None
    def inner_product(self, x, y):
        return np.dot(x.T.conj(), y)
# ==============================================================================
def newton(x0,
           model_evaluator,
           maxiter = 100,
           tol = 1.0e-10
           ):

    x = x0.copy()

    error_code = 1
    Fx = model_evaluator.compute_f(x)
    residuals = [np.linalg.norm(Fx)]
    k = 0
    print x
    while residuals[-1] > tol and k < maxiter:
        J = model_evaluator.get_jacobian(x)
        delta_x = np.linalg.solve(J, -Fx)
        x += delta_x
        print x

        Fx = model_evaluator.compute_f(x)
        residuals.append(np.linalg.norm(Fx))
        k += 1

    if residuals[-1] < tol:
        error_code = 0
    print residuals
    asasd
    return {'info': error_code,
            'x': x,
            'Newton residuals': residuals}
# ==============================================================================
def gauss_seidel(x0,
                 model_evaluator,
                 maxiter = 100,
                 tol = 1.0e-10
                 ):
    '''Nonlinear Gauss--Seidel for the CubicModelEvalutator.'''

    x = x0.copy()
    #x[0] = 1.0001
    #x[1] = 0

    error_code = 1
    Fx = model_evaluator.compute_f(x)
    residuals = [np.linalg.norm(Fx)]
    k = 0
    while residuals[-1] > tol and k < maxiter:
        print x.T
        # Fix x[1] and calculate the solutions of
        # the first equation only.
        r = np.roots([1,0,-3*x[1]**2,-1])
        # Pick out one real-valued solution
        # and reset x[0].
        real_r = r[abs(r.imag)<1.0e-13].real
        i = np.argmin(abs(x[0]-real_r))
        x[0] = real_r[i]

        # Do the same for x[1].
        # Here, the solutions are analytically
        # available.
        r[0] = 0.0
        r[1] = np.sqrt(3.0) * x[0]
        r[2] = -np.sqrt(3.0) * x[0]
        # Arbitrarily pick r[1].
        x[1] = r[1]

        Fx = model_evaluator.compute_f(x)
        residuals.append(np.linalg.norm(Fx))
        k += 1

    if residuals[-1] < tol:
        error_code = 0
    print residuals
    asasd
    return {'info': error_code,
            'x': x,
            'Newton residuals': residuals}
# ==============================================================================
def _main():
    '''Compute the Newton fractal of a given model evaluator.'''
    #model_eval = CubicModelEvaluator()
    model_eval = NlsModelEvaluator()

    #the solutions we are interested in
    sols = [np.array([[1],[0]]),
            np.array([[np.cos(2.0/3.0*np.pi)], [np.sin(2.0/3.0*np.pi)]]),
            np.array([[np.cos(-2.0/3.0*np.pi)], [np.sin(-2.0/3.0*np.pi)]])]
    # and their hues (for plotting)
    hues = np.array([0, 1, 2]) / 3.0

    # Define the box in which to compute.
    xmin = -1.5
    xmax = 1.5
    ymin = -1.5
    ymax = 1.5

    # Resolution.
    # "dots per inch", inch = unit length.
    dpi = 200
    m = int(round((xmax-xmin) * dpi))
    n = int(round((ymax-ymin) * dpi))

    print 'Calculating image of size %d x %d...' % (m,n)

    xpixelwidth = (xmax-xmin) / m
    ypixelwidth = (ymax-ymin) / n
    num_iters = np.empty((m,n), dtype=int)
    conv_point = np.empty((m,n), dtype=int)
    for i in range(m):
        print i
        x0 = xmin+xpixelwidth/2 + i*xpixelwidth
        for j in xrange(n):
            y0 = ymin+ypixelwidth/2 + j*ypixelwidth
            x0 = 1.0
            y0 = 1.0
            #out = gauss_seidel(np.array([[x0],[y0]]),
            #                   model_eval,
            #                   maxiter = 50,
            #                   tol = 1.0e-10)
            out = newton(np.array([x0,y0]),
                         model_eval,
                         tol = 1.0e-10,
                         maxiter = 50)
            print out['Newton residuals']
            if out['info'] == 0: # success
                if np.linalg.norm(out['x']-sols[0]) < 1.0e-5:
                    conv_point[i,j] = 0
                elif np.linalg.norm(out['x']-sols[1]) < 1.0e-5:
                    conv_point[i,j] = 1
                elif np.linalg.norm(out['x']-sols[2]) < 1.0e-5:
                    conv_point[i,j] = 2
                else:
                    raise RuntimeError('\nConverged to unknown \n%r.' % out['x'])

                num_iters[i,j] = len(out['Newton residuals']) - 1
            else:
                # no convergence
                num_iters[i,j] = -1
                conv_point[i,j] = 0 # not used anyways

    # Save the color values to a file.
    np.savetxt('num_iters.txt', num_iters)

    rgb = np.empty((m,n,3))
    # Construct and print the RGB array.
    for i in range(m):
        for j in xrange(n):
            if num_iters[i,j] < 0:
                rgb[i,j,:] = [1,1,1]
            else:
                rgb[i,j,:] = colorsys.hls_to_rgb(hues[conv_point[i,j]],
                                                 # Map num_iters in to [0,1) and make sure that this
                                                 # function resembles f(x)=x around x=0.
                                                 np.arctan(0.5*np.pi * num_iters[i,j]) / (0.5*np.pi),
                                                 1.0) # saturation

    # Print the image.
    scipy.misc.imsave('outfile.png', rgb[:,::-1,:].T)

    return
# ==============================================================================
if __name__ == '__main__':
  _main()
# ==============================================================================
