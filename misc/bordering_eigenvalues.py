# ==============================================================================
import numpy as np
import scipy
from scipy import linalg
# ==============================================================================
def _main():

    n = 3

    # Create arbitrary Hermitian matrix.
    A = np.random.rand(n,n) + 1j * np.random.rand(n,n)
    A = 0.5 * (A + A.T.conj())

    lambd, U = np.linalg.eigh(A)

    # Map one eigenvalue to 0.
    A -= lambd[0] * np.eye(n,n)

    # Check out the eigenvalues
    lambdA, UA = np.linalg.eigh(A)
    print 'sigma(A) =', lambdA

    # Deflation
    u = 1.0 * UA[:,0].reshape(n,1)
    # 'v' can also be chosen to equal u; in that case,
    # the matrices B and C will be Hermetian.
    v = np.random.rand(n,1) + 1j * np.random.rand(n,1)

    B = A + np.outer(u, v.T.conj())
    lambdB, UB = np.linalg.eig(B)
    print 'sigma(B) =', lambdB

    # Now border the system with the corresponding eigenvector.
    d = 10.0
    C = np.bmat([[A,          u],
                 [v.T.conj(), [[d]]]])
    lambdC, UC = np.linalg.eig(C)

    print
    print 'Predicted eigenvalues:'
    print 0.5*d + np.sqrt( (0.5*d)**2 + np.vdot(v,u) )
    print 0.5*d - np.sqrt( (0.5*d)**2 + np.vdot(v,u) )
    print

    print 'sigma(C) =', lambdC

    return
# ==============================================================================
if __name__ == '__main__':
  _main()
# ==============================================================================
