# ==============================================================================
import numpy as np
import scipy
from scipy import linalg
# ==============================================================================
def _main():
    '''Check those dreaded edge coefficients.'''
    dim = 3

    # Define n random vectors.
    n = 6
    e = np.random.rand(n, dim)

    alpha = _compute_edge_coefficients(e)

    A = np.zeros((dim, dim))
    for i in xrange(n):
        A += alpha[i] * np.outer(e[i], e[i])

    # Check the statement.
    diff = np.linalg.norm(A - np.eye(dim))
    print A
    print
    print 'diff:', diff

    return
# ==============================================================================
def _compute_edge_coefficients(edges):
    '''Build edge coefficients.'''
    # Build the equation system.
    # The equation
    #
    # ||u||^2 = \sum_i \alpha_i <u,e_i> <e_i,u>
    #
    # has to hold for all vectors u in the plane spanned by the edges,
    # particularly by the edges themselves.
    A = np.dot(edges, edges.T)
    # Careful here! As of NumPy 1.7, np.diag() returns a view.
    rhs = np.diag(A).copy()
    A = A**2

    # Append the the resulting coefficients to the coefficient cache.
    # The system is posdef iff the simplex isn't degenerate.
    return linalg.solve(A, rhs, sym_pos=True)
# ==============================================================================
if __name__ == '__main__':
  _main()
# ==============================================================================
