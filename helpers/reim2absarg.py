#!/usr/bin/env python
'''
Module to read a state file with real and imaginary parts of the state psi,
and write the same data out as abs(psi)^2 and arg(psi).
This is mainly intended for visualization.
'''
import mesh_io
import sys
from cmath import phase
import numpy as np
# ==============================================================================
def _main():
    infile, outfile = _parse_options()

    # read file
    mesh, psi, field_data = mesh_io.read_mesh( infile )

    # convert into abs^2, arg
    num_elems = len( psi )
    array_abs = np.empty( num_elems, dtype = float )
    array_arg = np.empty( num_elems, dtype = float )
    for k in xrange( len(psi) ):
        array_abs[k] = abs(psi[k])**2
        array_arg[k] = phase( psi[k] )

    # write file
    mesh_io.write_mesh( outfile,
                        mesh,
                        [ array_abs,    array_arg  ],
                        [ "abs(psi)^2", "arg(psi)" ],
                        field_data
                      )

    return
# ==============================================================================
def _parse_options():
    '''Parse input options.
    '''
    import optparse

    usage = "usage: %prog infile outfile"

    parser = optparse.OptionParser( usage = usage )

    (options, args) = parser.parse_args()

    if not args:
        parser.print_help()
        sys.exit( "\nProvide a file to be read." )

    return args
# ==============================================================================
if __name__ == "__main__":
    _main()
# ==============================================================================
