#!/usr/bin/env python

import numpy as np
from numerical_methods import cg_wrap
# ==============================================================================
N = 10

# create diagonal matrix
d = np.random.rand( N )
d[0] = -1
A = np.matrix( np.diag( d ) )

# create rhs
b = np.ones( (N,1) )

# initial guess
x0 = np.zeros( (N,1) )

# solve with cg
sol, info, relresvec = cg_wrap( A, b, x0, tol = 1.0e-5, maxiter = 1000 )

print info
print relresvec
# ==============================================================================