#! /usr/bin/env python
'''
Module to read an ASCII file of the fomat 
# X    Y    Z      Re         Im
  0.1  0.3  -0.2   3.4233e-1  6.8798e-1
  .    .    .      .          .
  .    .    .      .          .
  .    .    .      .          .

and write it to a VTU file. This works for rectangular meshes only, and the
nodes need to be ordered lexicographically.
'''
import vtkio, vtk
import mesh
import optparse
from math import sqrt
import numpy as np
import sys
# ==============================================================================
def _main():
    ascii_filename, vtu_filename = _parse_options()

    f = open(ascii_filename, 'rb')
    # read the parameter mu
    mu = float( f.readline() )

    # read the number of nodes
    num_nodes = int( f.readline() )

    # read the coordinates
    nodes = []
    for k in xrange( num_nodes ):
      nodes.append( mesh.Node( [float(s) for s in f.readline().split()] ) )

    # read the values
    psi = np.zeros( num_nodes, dtype = complex )
    for k in xrange( num_nodes ):
      z = [float(s) for s in f.readline().split()]
      psi[k] = complex( z[0], z[1] )
    # --------------------------------------------------------------------------
    # recreate the elements
    # create the elements (cells)
    nx = int(sqrt( num_nodes ))
    ny = nx
    if nx*ny != len( nodes ):
        raise ValueError( "\nGrid has " + str( len(nodes) )
                          + " nodes and hence is not square.\n"
                        )

    num_elems = 2 * (nx-1) * (ny-1)
    elems = []
    for i in range(nx - 1):
        for j in range(ny - 1):
            elems.append( mesh.Element( [ i*ny + j, (i + 1)*ny + j + 1,  i     *ny + j + 1 ],
                                        [], # edges
                                        vtk.VTK_TRIANGLE
                                      )
                        )
            elems.append( mesh.Element( [ i*ny + j, (i + 1)*ny + j    , (i + 1)*ny + j + 1 ],
                                        [], # edges
                                        vtk.VTK_TRIANGLE
                                      )
                        )
    # --------------------------------------------------------------------------
    # create the mesh data structure
    mymesh = mesh.Mesh( nodes, elems )

    # write the mesh with data
    vtkio.write_mesh( vtu_filename,mymesh,
                      X = psi,
                      name = "psi",
                      additional_field_data = { "mu": mu }
                    )

    return
# ==============================================================================
def _parse_options():
    '''
    Parse input options.
    '''
    usage = "usage: %prog asciifile vtufile"

    parser = optparse.OptionParser( usage = usage )

    (options, args) = parser.parse_args()

    if not args:
        parser.print_help()
        sys.exit( "\nProvide a file to be read." )
    elif len(args) < 2:
        parser.print_help()
        sys.exit( "\nProvide a file to write to." )

    return args[0], args[1]
# ==============================================================================
if __name__ == "__main__":
    _main()
# ==============================================================================
