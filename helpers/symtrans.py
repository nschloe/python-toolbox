#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Provides symmetry transformations on the square.

"""
import sys
import numpy as np

# ==============================================================================
def rot90( psi ):
    return np.rot90(psi)
# ==============================================================================
def rot180( psi ):
    return np.rot90( np.rot90(psi) )
# ==============================================================================
def conjugated_fliplr( psi ):
    return np.fliplr(psi).conjugate()
# ==============================================================================
def fliplr( psi ):
    return np.fliplr(psi)
# ==============================================================================
def conjugated_flipud( psi  ):
    return flipud(psi).conjugate()
# ==============================================================================
def flipud( psi ):
    return np.flipud(psi)
# ==============================================================================
def diagonal1( psi ):
    return np.rot90( fliplr(psi).conjugate() )
# ==============================================================================
def diagonal2( psi ):
    return np.rot90( flipud(psi).conjugate() )
# ==============================================================================
def _main():
    """
    Main function.
    """

    infile = '../test/data/mario1.vti'
    outfile = 'test.vti'

    try:
        psi, bounds = vtkio.reader( infile )
    except:
        sys.exit( "Could not open file '%s'." % infile )

    phi = conjugated_flipud( psi  )

    vtkio.writer( outfile, phi, bounds )

    return 0
# ==============================================================================
if __name__ == '__main__':
    sys.exit( _main() )
# ==============================================================================