#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Check which symmetries are present in a state given by a VT{I,K} file.
"""
import sys
from optparse import OptionParser # handle command line arguments
import numpy as np
from math import sqrt

import vtkio
import symtrans as st
# ==============================================================================
def _check_rot90( psi, h ):
    return l2_norm( psi-st.rot90(psi), h )
# ==============================================================================
def _check_rot180( psi,h  ):
    return l2_norm( psi-st.rot180(psi), h )
# ==============================================================================
def _check_conjugated_fliplr( psi,h  ):
    return l2_norm( psi - st.conjugated_fliplr(psi), h )
# ==============================================================================
def _check_fliplr( psi, h ):
    return l2_norm( psi - st.fliplr(psi), h )
# ==============================================================================
def _check_conjugated_flipud( psi,h  ):
    return l2_norm( psi - st.conjugated_flipud(psi), h )
# ==============================================================================
def _check_flipud( psi, h ):
    return l2_norm( psi - st.flipud(psi), h )
# ==============================================================================
def _check_diagonal1( psi, h ):
    return l2_norm( psi - st.diagonal1(psi), h )
# ==============================================================================
def _check_diagonal2( psi, h ):
    return l2_norm( psi - st.diagonal2(psi), h )
# ==============================================================================
def l2_norm( psi, h ):
    """L^2(\Omega)-norm of a given state psi."""
    return sqrt( h.prod() * ( psi.conjugate() * psi ).sum().real )
# ==============================================================================
def _main():
    """
    Main function.
    """
    tol = 1.0e-2
    # --------------------------------------------------------------------------
    # parse the options
    usage = "usage: %prog statefile"

    description = "Returns the list of symmetries to be found in the state " \
                  "given in the VT{I,K} file argument."

    parser = OptionParser( usage = usage,
                           description = description )

    (options, args) = parser.parse_args()
    # --------------------------------------------------------------------------
    if not args:
        parser.print_help()
        sys.exit()
    # --------------------------------------------------------------------------
    # read the state from file
    psi, bounds = vtkio.reader( args[0] )
    dims = psi.shape
    h = np.array([ (bounds[1]-bounds[0]) / (dims[0]-1),
                   (bounds[3]-bounds[2]) / (dims[1]-1) ] )
    # --------------------------------------------------------------------------
    symmetries = []
    rej_symmetries = []
    # --------------------------------------------------------------------------
    alpha = _check_rot90( psi, h )
    string = "ROT 90 (residual %g)" % alpha
    if alpha < tol:
        symmetries.append( string )
    else:
        rej_symmetries.append( string )
        alpha = _check_rot180( psi, h )
        string = "ROT 180 (residual %g)" % alpha
        if alpha < tol:
             symmetries.append( string )
        else:
             rej_symmetries.append( string )

    alpha = _check_conjugated_fliplr( psi, h )
    string = "conjugate flip LEFT-RIGHT (residual %g)" % alpha
    if alpha < tol:
        symmetries.append( string )
    else:
        rej_symmetries.append( string )

    alpha = _check_fliplr( psi, h )
    string = "flip LEFT-RIGHT (residual %g)" % alpha
    if alpha < tol:
        symmetries.append( string )
    else:
        rej_symmetries.append( string )

    alpha = _check_conjugated_flipud( psi, h )
    string = "conjugate flip UP-DOWN (residual %g)" % alpha
    if alpha < tol:
        symmetries.append( string )
    else:
        rej_symmetries.append( string )

    alpha = _check_flipud( psi, h )
    string = "flip UP-DOWN (residual %g)" % alpha
    if alpha < tol:
        symmetries.append( string )
    else:
        rej_symmetries.append( string )

    alpha = _check_diagonal1( psi, h )
    string = "diagonal left-top right-bottom (residual %g)" % alpha
    if alpha < tol:
        symmetries.append( string )
    else:
        rej_symmetries.append( string )

    alpha = _check_diagonal2( psi, h )
    string = "diagonal left-bottom right-top (residual %g)" % alpha
    if alpha < tol:
        symmetries.append( string )
    else:
        rej_symmetries.append( string )
    # --------------------------------------------------------------------------
    if symmetries:
        print "Symmetries of the state:\n"
        for sym in symmetries:
            print "\t"+sym
        if rej_symmetries:
            print "\n"

    if rej_symmetries:
        print "Rejected symmetries of the state:\n"
        for sym in rej_symmetries:
            print "\t"+sym
    # --------------------------------------------------------------------------
    return 0
# ==============================================================================
if __name__ == '__main__':
    sys.exit( _main() )
# ==============================================================================