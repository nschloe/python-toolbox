#! /usr/bin/env python
# -*- coding: utf-8 -*-
# ==============================================================================
import optparse, cmath, math
import numpy as np
from PIL import Image

import vtkio
# ==============================================================================
# COMMAND-LINE ARGUMENTS
def command_line_options():
    # --------------------------------------------------------------------------
    # create parser
    usage = "usage: %prog real-part-file imag-part-file "
    version_string = "0.0.1\n"
    # TODO place proper license here

    desc = "Transforms images files into VTK XML Image files."

    parser = optparse.OptionParser( usage = usage,
                                    version = version_string,
                                    description = desc )
                                    
    parser.add_option( "-e", "--edgelength",
                      action="store", type="float", dest="edge_length",
                      default = 1.0,
                      help="edge length of the domain", metavar="EDGELENGTH")
                      
    parser.add_option( "-o", "--outfile",
                      action="store", type="string", dest="outfile",
                      default = "out.vti",
                      help="file name for the output", metavar="OUTFILE")
    # --------------------------------------------------------------------------
    # parse and post-process options
    (options, args) = parser.parse_args()
    # --------------------------------------------------------------------------
    if len(args) != 2:
        parser.error( "Need exactly two arguments." )

    return (options, args)
# ==============================================================================
def read_png( filename ):

    pic = Image.open( filename )
    arr = np.array( pic )
    
    if len(arr.shape)==3 and arr.shape[2] == 3:
        # RGB colors; translate into array
        new_arr = np.zeros( arr.shape[0:2] )
        for i in range( arr.shape[0] ):
             for j in range( arr.shape[1] ):
                 new_arr[i, j] = np.average( arr[i,j,:] ) / 255.
        arr = new_arr
    elif len(arr.shape)==2:
        # grayscale data
        arr = arr / 255.
    
    return arr
# ==============================================================================
def scale_array( array, scale ):
    """
    Scale values in array to  [ scale[0], scale[1] ].
    """
    minval = float( array.min() )
    maxval = float( array.max() )
    return (array-minval) * (scale[1]-scale[0]) / (maxval-minval) + scale[0]
# ==============================================================================
def create_complex_array( real, imag ):

    psi = np.ndarray( shape = real.shape,
                      dtype = complex )

    for i in range(real.shape[0]):
        for j in range(real.shape[1]):
            psi[i, j] = cmath.rect( math.sqrt(real[i, j]),
                                    imag[i, j] )

    return psi
# ==============================================================================
if __name__ == "__main__":
    (OPTIONS, ARGS) = command_line_options()

    # bounds for the square
    BOUNDS = [ 0, OPTIONS.edge_length ]
    
    #vtkio.reader( 'init.vti' )

    # read parts
    REAL_PART = read_png( ARGS[0] )
    #REAL_PART = scale_array( REAL_PART, [0,1] )
    IMAG_PART = read_png( ARGS[1] )
    #IMAG_PART = scale_array( IMAG_PART, [-pi,pi] )
    PSI = create_complex_array( REAL_PART, IMAG_PART )

    #fig = figure()
    #bounds = [0,10,0,10]
    #plothelpers.plot_solution( psi, bounds, fig )
    #show()
    additional_field_data = { "H0": [0.0] }
    vtkio.writer( OPTIONS.outfile, PSI, BOUNDS, additional_field_data )
# ==============================================================================
