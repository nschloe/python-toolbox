#! /usr/bin/env python
# -*- coding: utf-8 -*-
'''
Takes a given state as VTI/VTK file with n-by-m vortices, and generates an
interpolated state with N by M vortices and the same domain dimensions.
'''
# ==============================================================================
import optparse
import numpy as np
import math
import vtkio
# ==============================================================================
# COMMAND-LINE ARGUMENTS
def command_line_options():
    # --------------------------------------------------------------------------
    # create parser
    usage = "usage: %prog infile new-dimension outfile"
    version_string = "0.0.1\n"

    desc = "Takes a given state as VTI/VTK file with n-by-m vortices, and generates an" \
           "interpolated state with N by M vortices and the same domain dimensions. ."

    parser = optparse.OptionParser( usage = usage,
                                    version = version_string,
                                    description = desc )
                                    
    #parser.add_option( "-e", "--edgelength",
                      #action="store", type="float", dest="edge_length",
                      #default = 1.0,
                      #help="edge length of the domain", metavar="EDGELENGTH")
                      
    #parser.add_option( "-o", "--outfile",
                      #action="store", type="string", dest="outfile",
                      #default = "out.vti",
                      #help="file name for the output", metavar="OUTFILE")
    # --------------------------------------------------------------------------
    # parse and post-process options
    (options, args) = parser.parse_args()
    # --------------------------------------------------------------------------
    if len(args) != 3:
        parser.error( "Need exactly three arguments." )

    infile, newdim, outfile = args
    return infile, int(newdim), outfile
# ==============================================================================
def bilinear_interpolation( psi, N ):

    psi_new = np.zeros( [N,N], dtype=complex )
   
    print psi.shape
    m, n = psi.shape
    
    for i in range(N):
        for j in range(N):
            #print (i*m) % N, (j*n) % N
            if (i*(m-1)) % (N-1) == 0  and  (j*(n-1)) % (N-1) == 0:
                # Point sits exactly in an old (i,j) node.
                # --> No interpolation necessary at all
                i_old = i * (m-1) / (N-1)
                j_old = j * (n-1) / (N-1)
                psi_new[ i, j ] = psi[ i_old, j_old ]
                
            elif (i*(m-1)) % (N-1) == 0:
                # Point sits exatcly on an old i-line.
                # --> Just interpolate in j-direction
                i_old = i * (m-1) / (N-1)
                j_old = int(math.floor( float(j) * (n-1) / (N-1) ))
                
                j_weight  = float( (N-1)*(j_old+1) - (n-1)*j     ) / N
                j1_weight = float( (n-1)*j         - (N-1)*j_old ) / N
                
                
                psi_new[ i, j ] = j_weight  * psi[ i_old, j_old   ] \
                                + j1_weight * psi[ i_old, j_old+1 ]

            elif (j*(n-1)) % (N-1) == 0:
                # Point sits exatcly on an old j-line.
                # --> Just interpolate in i-direction
                j_old = j * (n-1) / (N-1)
                i_old = int(math.floor( float(i) * (m-1) / (N-1) ))
                
                i_weight  = float( (N-1)*(i_old+1) - (m-1)*i     ) / (N-1)
                i1_weight = float( (m-1)*i         - (N-1)*i_old ) / (N-1)
                
                psi_new[ i, j ] = i_weight  * psi[ i_old  , j_old ] \
                                + i1_weight * psi[ i_old+1, j_old ]

            else:
                # full bilinear interpolation
                i_old = int(math.floor( float(i*(m-1)) / (N-1) ))
                j_old = int(math.floor( float(j*(n-1)) / (N-1) ))
                
                ij_weight   = float( ((N-1)*(i_old+1) - (m-1)*i    ) * ((N-1)*(j_old+1) - (n-1)*j    ) ) / ((N-1)*(N-1))
                i1j_weight  = float( ((m-1)*i         - (N-1)*i_old) * ((N-1)*(j_old+1) - (n-1)*j    ) ) / ((N-1)*(N-1))
                ij1_weight  = float( ((N-1)*(i_old+1) - (m-1)*i    ) * ((n-1)*j         - (N-1)*j_old) ) / ((N-1)*(N-1))
                i1j1_weight = float( ((m-1)*i         - (N-1)*i_old) * ((n-1)*j         - (N-1)*j_old) ) / ((N-1)*(N-1))
                
                psi_new[ i, j ] = ij_weight   * psi[ i_old  , j_old   ] \
                                + i1j_weight  * psi[ i_old+1, j_old   ] \
                                + ij1_weight  * psi[ i_old  , j_old+1 ] \
                                + i1j1_weight * psi[ i_old+1, j_old+1 ]
                                
    return psi_new
# ==============================================================================
if __name__ == "__main__":
    infile, newdim, outfile = command_line_options()
    
    # read the original file
    mesh, psi, field_data = vtkio.read_mesh( infile )

    # make sure psi sits on a square grid
    n = int( round( math.sqrt( len(psi) ) ) )
    assert n*n == len(psi)

    # reshape to square
    psi = psi.reshape( (n,n) )

    # interpolate to new resolution
    psi_new = bilinear_interpolation ( psi, newdim )
    
    # put psi_new into a vector
    psi_new = psi_new.reshape( (newdim*newdim,1) )

    # write psi_new back to a file
    vtkio.write_mesh( outfile,
                      mesh,
                      psi_new,
                      "psi",
                      field_data
                    )
# ==============================================================================
