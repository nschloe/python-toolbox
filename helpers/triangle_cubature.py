#! /usr/bin/env python
'''
Integrates any given quadratic function over any triangle.
'''
import voropy
#import magnetic_vector_potentials
import numpy as np
# ==============================================================================
def _main():
    # Parse command line arguments.
    args = _parse_options()

    points = np.array([[0,0], [1,0], [0.5,0.2], [0.5,-0.2]])
    #cells = np.array([[0,2,3],[1,2,3]])
    cells = np.array([[0,1,2],[0,1,3]])

    degree = 5
    qpoints, qweights = get_quadrature_points_weights(degree)

    for h in [1.0, 0.5, 0.25, 0.125]:
        scaled_points = h * points
        # Function to integrate
        def f(x):
            return np.cos(x[0]) * np.cos(x[1])
            #a = [6,5,4,1,2,3]
            #return a[0]*x[0]**2 + a[1]*x[1]**2 + a[2]*x[0]*x[1] \
                 #+ a[3]*x[0] + a[4]*x[1]+ a[5]

        qsum = quadrature_sum(f, scaled_points[cells], qpoints, qweights)
        print 'Quadrature sum:', qsum

        # Compare this with the finite volume approximation.
        mesh = voropy.mesh2d(scaled_points, cells)
        if mesh.control_volumes is None:
            mesh.compute_control_volumes()
        csum = 0.0
        for cv, point in zip(mesh.control_volumes, points):
            csum += cv * f(point)
        print 'Total control volume sum:', csum
        print 'Relative difference between the two:', abs(qsum - csum) / qsum
        print
    return
# ==============================================================================
def quadrature_sum(f, triangle_nodes, qpoints, qweights):
    '''Computes the quadrature of f over the triangle defined by triangle_nodes
    with the quadrature points qpoints and weights qweights.
    '''
    total_sum = 0.0
    for triangle in triangle_nodes:
        det = referenceMappingDet(triangle)
        def g(x):
            return f(reference2tri(x, triangle)) * abs(det)

        sum = 0.0
        for qpoint, qweight in zip(qpoints, qweights):
            sum += qweight * g(qpoint)
        total_sum += sum
        #print 'Quadrature triangle contribution:', sum[0]
    return total_sum[0]
# ==============================================================================
def reference2tri(x, tri):
    '''Maps a point in the reference triangle to the triangle tri.
    '''
    J = np.array([[tri[1][0] - tri[0][0], tri[2][0] - tri[0][0]],
                  [tri[1][1] - tri[0][1], tri[2][1] - tri[0][1]]])
    return np.dot(J, x) + tri[0]
# ==============================================================================
def referenceMappingDet(tri):
    '''Determinant of the Jacobian of the reference mapping.
    '''
    return np.linalg.det(np.array([[tri[1][0] - tri[0][0], tri[2][0] - tri[0][0]],
                                   [tri[1][1] - tri[0][1], tri[2][1] - tri[0][1]]]))
# ==============================================================================
def get_quadrature_points_weights(n):
    '''Degree n quadrature points and weights on a triangle (0,0)-(1,0)-(0,1).
    From http://nullege.com/codes/search/scipy.special.orthogonal.p_roots.
    '''
    import scipy.special.orthogonal
    x00, w00 = scipy.special.orthogonal.p_roots(n)
    x01, w01 = scipy.special.orthogonal.j_roots(n,1,0)
    x00s = (x00+1)/2
    x01s = (x01+1)/2
    w = np.outer(w01, w00).reshape(-1,1) / 8 # a factor of 2 for the legendres and 4 for the jacobi10
    x = np.outer(x01s, np.ones(x00s.shape)).reshape(-1,1)
    y = np.outer(1-x01s, x00s).reshape(-1,1)
    return np.hstack((x, y)), w
# ==============================================================================
def _parse_options():
    '''Parse input options.'''
    import argparse

    parser = argparse.ArgumentParser( description = 'Triangle cubature.' )

    return parser.parse_args()
# ==============================================================================
if __name__ == "__main__":
    _main()
# ==============================================================================
