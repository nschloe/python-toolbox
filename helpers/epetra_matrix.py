#! /usr/bin/env python
'''
Read an Epetra matrix from a file and builds a Scipy matrix from it.
'''
import numpy as np
import scipy.sparse
# ==============================================================================
def _main():
    # Parse command line arguments.
    args = _parse_options()

    data = np.loadtxt(args.filename)

    # The Epetra format is:
    # [proc, row, column, value].
    ij = data[:,1:3].astype(int).T
    print ij.shape
    A = scipy.sparse.csr_matrix((data[:,3], ij))
    print A
    print
    print block2complex(A)

    return
# ==============================================================================
def block2complex( A ):
    '''Converts a 2x2 block matrix
        Re -Im
        Im  Re
    to its corresponding complex-valued representation.
    '''
    m, n = A.shape
    assert m == n
    assert m % 2 == 0

    data = []
    I = []
    J = []
    for i in xrange(m/2):
        for j in xrange(m/2):
            # Check the block.
            assert A[2*i,2*j] == A[2*i+1, 2*j+1]
            assert A[2*i+1,2*j] == -A[2*i, 2*j+1]
            x = A[2*i,2*j] + 1j * A[2*i+1,2*j]
            if abs(x) > 1.0e-13:
                I.append(i)
                J.append(j)
                data.append(x)

    return scipy.sparse.csr_matrix((np.array(data), np.array([I,J])))
# ==============================================================================
def _parse_options():
    '''Parse input options.'''
    import argparse

    parser = argparse.ArgumentParser( description = 'Read Epetra matrices.' )

    parser.add_argument( 'filename',
                         type    = str,
                         help    = 'file that contains matrix data'
                       )

    return parser.parse_args()
# ==============================================================================
if __name__ == "__main__":
    _main()
# ==============================================================================
