#! /usr/bin/env python
import voropy
import numpy as np
# ==============================================================================
def _main():
    # Parse command line arguments.
    args = _parse_options()

    # read mesh
    mesh, point_data, field_data = voropy.read(args.filename, timestep=args.timestep)

    if point_data:
        print 'Point data'
        print '=========='
        for key, value in point_data.items():
            print '%s:' % key
            print value
            print
    else:
        print 'No point data.'
    print

    if field_data:
        print 'Field data'
        print '=========='
        for key, value in field_data.items():
            print '%s:' % key
            try:
                print ''.join(value)
            except TypeError:
                print value
            print
    else:
        print 'No field data.'

    return
# ==============================================================================
def _parse_options():
    '''Parse input options.'''
    import argparse

    parser = argparse.ArgumentParser( description = 'Show field and point data.' )

    parser.add_argument( 'filename',
                         metavar = 'INFILE',
                         type    = str,
                         help    = 'mesh file to be read from'
                       )

    parser.add_argument( '--timestep', '-t',
                         metavar='TIMESTEP',
                         dest='timestep',
                         type=int,
                         default=0,
                         help='read a particular time step (default: 0)'
                       )

    return parser.parse_args()
# ==============================================================================
if __name__ == "__main__":
    _main()
# ==============================================================================
