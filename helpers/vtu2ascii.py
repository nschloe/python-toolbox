#! /usr/bin/env python
'''
Module to read a VTU file and write the state values out into an ASCII column
format as in
# X    Y    Z      Re         Im
  0.1  0.3  -0.2   3.4233e-1  6.8798e-1
  .    .    .      .          .
  .    .    .      .          .
  .    .    .      .          .
'''
import vtkio
import optparse
import sys
# ==============================================================================
def _main():
    vtu_filename = _parse_options()

    # read vtu file
    mesh, psi, field_data = vtkio.read_mesh ( vtu_filename )

    num_nodes = len( mesh.nodes )

    # print parameter mu
    print field_data['mu']

    # print number of nodes
    print num_nodes

    # print coordinates
    for k in xrange( num_nodes ):
        print '%e\t%e\t%e' % ( mesh.nodes[k].coords[0],
                               mesh.nodes[k].coords[1],
                               mesh.nodes[k].coords[2]
                             )

    # print values
    for k in xrange( num_nodes ):
        print '%e\t%e' % ( psi[k].real, psi[k].imag )

    return
# ==============================================================================
def _parse_options():
    '''
    Parse input options.
    '''
    usage = "usage: %prog vtufile asciifile"

    parser = optparse.OptionParser( usage = usage )

    (options, args) = parser.parse_args()

    if not args:
        parser.print_help()
        sys.exit( "\nProvide a file to be read." )

    return args[0]
# ==============================================================================
if __name__ == "__main__":
    _main()
# ==============================================================================