# ==============================================================================
import numpy as np
import scipy.sparse
from scipy.sparse.linalg import spsolve
import matplotlib.pyplot as pp
# ==============================================================================
def diffusion0( x ):
    return 1.0
# ==============================================================================
def exact_solution0( x ):
    return np.sin( np.pi*x )
# ==============================================================================
def rhs0( x ):
    return np.pi**2 * np.sin( np.pi*x )
# ==============================================================================
def diffusion1( x ):
    return np.sin( 3.0*np.pi*x ) * 0.5 + 1.0
# ==============================================================================
def exact_solution1( x ):
    return np.sin( np.pi*x )
# ==============================================================================
def rhs1( x ):
    return - 1.5 * np.pi**2 * np.cos( 3.0 * np.pi*x ) * np.cos( np.pi*x ) \
         + ( np.sin( 3.0*np.pi*x ) * 0.5 + 1.0 ) * np.pi**2 * np.sin( np.pi*x )
# ==============================================================================
def compute_solution( n ):
    # length of domain
    l = 1.0

    # construct grid
    x = np.linspace( 0, l, n+1 )

    h = l / n

    diff           = diffusion1
    exact_solution = exact_solution1
    rhs            = rhs1

    # construct equation system
    A = scipy.sparse.lil_matrix(( n+1, n+1))
    b = np.empty( n+1, dtype=float )
    for k in xrange( n+1 ):
        if k==0: # boundary condition left
            A[k,k] = 1.0
            b[k]   = 0.0
        elif k==n: # boundary condition right
            A[k,k] = 1.0
            b[k]   = 0.0
        else: # interior
            # matrix
            A[k,k-1] = - diff( 0.5*(x[k-1]+x[k]) ) / h**2
            A[k,k]   = ( diff( 0.5*(x[k-1]+x[k]) ) + diff(0.5*(x[k]+x[k+1]) ) ) / h**2
            #A[k,k+1] = - diff( 0.5*(x[k]+x[k+1]) ) / h**2

            #A[k,k-1] = - 0.5 * ( diff(x[k-1]) + diff(x[k]) ) / h**2
            #A[k,k]   = ( diff( 0.5*(x[k-1]+x[k]) ) + diff(0.5*(x[k]+x[k+1]) ) ) / h**2
            A[k,k+1] = - 0.5 * ( diff(x[k]) + diff(x[k+1]) ) / h**2

            # right hand side
            b[k] = rhs( x[k] )
        
    # convert to CSR storage and solve
    A.tocsr()
    sol = spsolve(A, b)

    err = sol - exact_solution(x)

    return x, h, sol, err
# ==============================================================================
def plot_solution():
    n = 100
    x, h, sol, err = compute_solution( n )

    # print the solution
    pp.plot( x, sol )
    pp.show()

    # print error
    pp.plot( x, err )
    pp.show()
    return
# ==============================================================================
def err():
    H = []
    Err = []
    for k in xrange( 0, 12 ):
        n = 2**k # number of boxes
        x, h, sol, err = compute_solution( n )

        H.append( h )
        Err.append( max(err) )

    # plot the error
    pp.loglog( H, Err, 'x' )
    pp.show()

    return
# ==============================================================================
if __name__=="__main__":
    err()
# ==============================================================================
