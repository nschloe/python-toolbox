#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""Script for plotting the continuation curves with stability information."""
# ==============================================================================
import subprocess
import os
import matplotlib.pyplot as pp
import matplotlib.image as mpimg
import numpy as np
from PIL import Image, ImageChops
# ==============================================================================
def trim(im):
    bg = Image.new(im.mode, im.size, im.getpixel((0,0)))
    diff = ImageChops.difference(im, bg)
    diff = ImageChops.add(diff, diff, 2.0, -100)
    bbox = diff.getbbox()
    if bbox:
        return im.crop(bbox)
# ==============================================================================
def _main():

    args = _parse_args()
    continuation_data = np.loadtxt( args.cont_file )

    # Read continuation data.
    x_values = continuation_data[:,args.columns[0]]
    y_values = continuation_data[:,args.columns[1]]

    # Create figures.
    fig = pp.figure(figsize=(12.0, 5.0))
    # First axes-pair: parameter data.
    ax1  = fig.add_subplot(1, 2, 1)
    pp.plot( x_values, y_values, 'k-' )
    pp.xlabel('$\mu$')
    pp.ylabel('Gibbs energy')
    pp.ylim( -1.0, -0.4 )
    dot = pp.plot(x_values[0], y_values[0], 'or')
    # Create second axes.
    ax2  = fig.add_subplot(1, 2, 2)
    ax2.set_frame_on(False)
    ax2.get_xaxis().set_visible(False)
    ax2.get_yaxis().set_visible(False)
    im = trim(Image.open(args.stateimgs[0]))
    # Print the flipped version for compatibility with matplotlib.image.
    im_plot = pp.imshow(im.transpose(Image.FLIP_TOP_BOTTOM), vmin=-np.pi, vmax=np.pi, cmap=pp.cm.hsv)
    cbar = fig.colorbar(im_plot)
    cbar.set_ticks([-np.pi, 0.0, np.pi])
    cbar.set_ticklabels(['$-\pi$', '$0$', '$\pi$'])

    # Create images.
    for k in xrange(len(args.stateimgs)):
        print '%d / %d' % (k, len(args.stateimgs))
        dot[0].set_xdata(x_values[k])
        dot[0].set_ydata(y_values[k])

        # Read and trim image.
        #_run('convert -trim %s tmp.png' % args.stateimgs[k])
        #img = mpimg.imread('tmp.png')
        im = trim(Image.open(args.stateimgs[k]))
        # Print the flipped version for compatibility with matplotlib.image.
        im_plot.set_data(im.transpose(Image.FLIP_TOP_BOTTOM))

        pp.savefig('diptych%04d.png' % k)
        #pp.show()
    # remove temp file
    #os.remove('tmp.png')

    # create movie
    _run('avconv -r 5 -i diptych%%04d.png -f webm %s' % args.out_file)

    return
# ==============================================================================
def _run( command ):
    """Runs a given command on the command line and returns its output.
    """
    print command

    process = subprocess.Popen( command,
                                shell     = True,
                                stdout    = subprocess.PIPE,
                                stderr    = subprocess.PIPE,
                                close_fds = True
                              )
    output = process.stdout.read()[:-1]
    ret = process.wait()

    if ret != 0:
        import sys
        sys.exit( "\nERROR: The command \n\n%s\n\nreturned a nonzero " \
                  "exit status. The error message is \n\n%s\n\n" \
                  "Abort.\n" % \
                  ( command, process.stderr.read()[:-1] )
                )
    return output
# ==============================================================================
def _parse_args():
    '''Parse input arguments.'''
    import argparse

    parser = argparse.ArgumentParser( description = 'Create animated plots.' )

    parser.add_argument( 'stateimgs',
                         metavar = 'FILES',
                         nargs = '+',
                         help = 'images from states'
                       )

    parser.add_argument( '--cont-file', '-f',
                         required = True,
                         type = str,
                         help = 'continuation data file'
                       )

    parser.add_argument( '--columns', '-c',
                         required = True,
                         type = int,
                         nargs = 2,
                         help = 'column indices to plot (0-based indexing)'
                       )

    parser.add_argument( '--out-file', '-o',
                         required = True,
                         type = str,
                         help = 'output animation file'
                       )

#    parser.add_argument('--timesteps', '-t',
#                         metavar='TIMESTEP',
#                         type=int,
#                         nargs = '+',
#                         default=None,
#                         help='read a particular time step/range (default: all)'
#                        )

    return parser.parse_args()
# ==============================================================================
if __name__ == '__main__':
    _main()
# ==============================================================================
