#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""Function to plot the continuation curves with stability information."""
# ------------------------------------------------------------------------------
import matplotlib2tikz

from matplotlib.pyplot import plot, show
from matplotlib.cm import gray
import numpy as np
import sys
from optparse import OptionParser
# ------------------------------------------------------------------------------
FOLDER = None
OPTIONS = None
# ------------------------------------------------------------------------------
def _cmd_options():

    usage = "Usage: %prog datafolder"
    desc = "Plots continuation diagram with stability information."

    # parse the options
    parser = OptionParser( usage = usage,
                           description = desc )
    parser.add_option( "-t", "--tikz",
                      action="store", type="string", dest="tikz",
                      help="Generate TikZ file FILE", metavar="FILE")

    global OPTIONS
    global FOLDER

    (OPTIONS, args) = parser.parse_args()

    if not args:
        parser.print_help()
        sys.exit()

    FOLDER = args[0]

    return
# ------------------------------------------------------------------------------
def get_plot_data( continuation_file_path,
                   eigen_file_path,
                   num_unstable_eigenvals_column,
                   plot_columns ):

    try:
        continuation_data = np.loadtxt( continuation_file_path )
    except IOError:
        sys.exit( "The file '" + continuation_file_path + "' does not exist." )

    try:
        # read as strings b/c some entries may be '---------'
        eigen_data = np.loadtxt( eigen_file_path,
                                dtype = str )
        num_unstable_eigenvalues = np.array([int(entry) for entry in \
                                  eigen_data[:, num_unstable_eigenvals_column]])
    except IOError:
        print "The file '" + eigen_file_path + \
              "' does not exist. Continue plotting without eigen data."
        num_unstable_eigenvalues = None

    # don't start from 0: compatibility with eigen_data!
    x_values = continuation_data[1:, plot_columns[0]]
    y_values = continuation_data[1:, plot_columns[1]]

    if num_unstable_eigenvalues is not None:
        stable_points   = np.ma.masked_where( num_unstable_eigenvalues > 0,
                                              y_values)
        unstable_points = np.ma.masked_where( num_unstable_eigenvalues == 0,
                                              y_values)
    else:
        stable_points = y_values
        unstable_points = None
                                              
    return x_values, stable_points, unstable_points
# ------------------------------------------------------------------------------
def _main():

    continuation_file_name = 'continuationData.dat'
    eigen_file_name = 'eigenvalues.dat'
    num_unstable_eigenvals_column = 1
    plot_columns = [ 1, 3 ]
    
    continuation_file_path = FOLDER + '/' + continuation_file_name
    eigen_file_path = FOLDER + '/' + eigen_file_name
    
    x_values, stable_points, unstable_points = get_plot_data( continuation_file_path,
                                                              eigen_file_path,
                                                              num_unstable_eigenvals_column,
                                                              plot_columns )

    if unstable_points is not None:
        plot( x_values, stable_points,   'r-', \
              x_values, unstable_points, 'r:' )
    else:
        plot( x_values, stable_points, 'r:' )

    if OPTIONS.tikz:
        matplotlib2tikz.save( OPTIONS.tikz )
    else:
        show()

    return
# ------------------------------------------------------------------------------
if __name__ == "__main__":
    _cmd_options()
    _main()
# ------------------------------------------------------------------------------
