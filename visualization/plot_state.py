#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Function to plot one single superconductor state
"""
# ==============================================================================
import sys

from optparse import OptionParser # handle command line arguments

import plothelpers
import vtkio

from matplotlib import pyplot
from matplotlib import rcParams
rcParams['text.usetex'] = True
rcParams['text.latex.unicode'] = True
# ==============================================================================
def main():
    """
    Main function.
    """
    # --------------------------------------------------------------------------
    # parse the options
    usage = "usage: %prog [options] statefile"

    parser = OptionParser( usage=usage )

    parser.add_option( "-t", "--tikz-file",
                       action="store",
                       type="string",
                       dest="output_tikz_file",
                       help="Plot the image as TikZ file",
                       metavar="TIKZFILE" )

    parser.add_option( "-w", "--tikz-width",
                       action="store",
                       type="string", dest="tikz_width",
                       help="If output to TikZ selected, this gives the " \
                            "width of the plot",
                       metavar="TIKZWIDTH" )

    parser.add_option( "-e", "--tikz-height",
                       action="store",
                       type="string",
                       dest="tikz_height",
                       default=None,
                       help="If output to TikZ selected, this gives the " \
                            "height of the plot",
                       metavar="TIKZHEIGHT")

    parser.add_option( "-p", "--pdf-file",
                       action="store",
                       type="string",
                       dest="output_pdf_file",
                       help="Plot the image as PDF file",
                       metavar="PDFFILE" )

    parser.add_option( "-o", "--dont-plot-order-parameter",
                       action="store_false",
                       dest="plot_order_parameter",
                       default=True,
                       help="Don't plot the order parameter" )

    parser.add_option( "-a", "--dont-plot-argument",
                       action="store_false",
                       dest="plot_argument",
                       default=True,
                       help="Don't plot the argument" )

    parser.add_option( "-v", "--vorticity-highlight",
                       action="store_true",
                       dest="vorticity_highlight",
                       default=False,
                       help="Highlight the vortices with vorticity" )

    (options, args) = parser.parse_args()
    # --------------------------------------------------------------------------
    if not args:
        parser.print_help()
        sys.exit( "Provide a file to be read." )
    # --------------------------------------------------------------------------
    fig = pyplot.figure()

    # read the state from file
    psi, bounds, h, field_data = vtkio.reader( args[0] )

    if options.plot_order_parameter and options.plot_argument: # plot both
        plothelpers.plot_solution( psi, bounds, fig )
    elif options.plot_order_parameter: # only plot order parameter
        plothelpers.plot_psi_op( psi, bounds )
    elif options.plot_argument: # only plot argument
        plothelpers.plot_psi_arg( psi, bounds )
    else:
        return

    if options.vorticity_highlight:
        plothelpers.mark_vortices( psi, bounds )

    if options.output_tikz_file:
        import matplotlib2tikz
        matplotlib2tikz.save( options.output_tikz_file,
                              figurewidth  = options.tikz_width,
                              figureheight = options.tikz_height,
                              strict = True )

    if options.output_pdf_file:
        pyplot.savefig( options.output_pdf_file )

    if not options.output_tikz_file and not options.output_pdf_file:
        pyplot.show()

    return 0
# ==============================================================================
if __name__ == '__main__':
    sys.exit( main() )
# ==============================================================================
