#! /usr/bin/env python
'''Plots column data from a file Gnuplot-style with the exception that this
routine can handle multiple files at once.
'''
# ==============================================================================
import numpy as np
import matplotlib.pyplot as pp
# ==============================================================================
def _main():
    args = _parse_input_arguments()

    # Gather the data.
    if len(args.filenames) == 1:
       content = np.loadtxt(args.filenames[0])
       data0 = content[:args.maxrow,args.columns[0]]
       data1 = content[:args.maxrow,args.columns[1]]
    elif len(args.filenames) == 2:
       content0 = np.loadtxt(args.filenames[0])
       content1 = np.loadtxt(args.filenames[1])
       if len(content0.shape) == 1:
           data0 = content0[:args.maxrow]
       else:
           data0 = content0[:args.maxrow,args.columns[0]]
       if len(content1.shape) == 1:
           data1 = content1[:args.maxrow]
       else:
           data1 = content1[:args.maxrow,args.columns[1]]
    else:
        raise ValueError('Can only read from either one or two files')

    # Plot it.
    idx = -data1 > args.threshold
    pp.plot(data0[idx], data1[idx], args.style)

    if not args.output is None:
        import matplotlib2tikz
        print args.output
        matplotlib2tikz.save(args.output)
    else:
        # Show the plot.
        pp.show()

    return
# ==============================================================================
def _parse_input_arguments():
    '''Parse input arguments.
    '''
    import argparse

    parser = argparse.ArgumentParser( description = 'Plot column data from files Gnuplot style.' )

    parser.add_argument('filenames',
                        metavar = 'FILES',
                        nargs='+',
                        type=str,
                        help='file names containing the column data'
                        )

    parser.add_argument('--columns', '-c',
                        required=True,
                        nargs=2,
                        type=int,
                        help='which two columns to read from'
                        )

    parser.add_argument('--maxrow', '-m',
                        type=int,
                        default=-1,
                        help='maximum row number until which to plot'
                        )

    parser.add_argument('--threshold', '-t',
                        type=int,
                        default=None,
                        help='coordinate data to skip'
                        )

    parser.add_argument('--output', '-o',
                        metavar = 'OUTFILE',
                        default=None,
                        type=str,
                        help='output to TikZ/Pgfplots file'
                        )

    parser.add_argument('--style', '-s',
                        default='-k',
                        type=str,
                        help='TikZ/Pgfplots style (default: \'-k\')'
                        )

    args = parser.parse_args()

    return args
# ==============================================================================
if __name__ == '__main__':
    _main()
# ==============================================================================
