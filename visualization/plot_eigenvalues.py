#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""Function to prettyplot eigenvalue data."""
# ------------------------------------------------------------------------------
import matplotlib2tikz

import matplotlib.pyplot as plt

import numpy as np
import sys
from optparse import OptionParser
# ------------------------------------------------------------------------------
EIGENDATAFILE = None
OPTIONS = None
# ------------------------------------------------------------------------------
def _cmd_options():

    usage = "Usage: %prog eigendata-file"
    desc = "Plots eigendata (of a continuation run) " \
           "in a visually appealing manner."

    # parse the options
    parser = OptionParser( usage = usage,
                           description = desc )
    parser.add_option( "-t", "--tikz",
                      action="store", type="string", dest="tikz",
                      help="Generate TikZ file FILE", metavar="FILE")
    
    (options, args) = parser.parse_args()

    if not args:
        parser.print_help()
        sys.exit()

    return args[0], options.tikz
# ------------------------------------------------------------------------------
def _main():

    eigendatafile, tikzfile = _cmd_options()

    try:
        file_content = open( eigendatafile, 'r' )
    except IOError:
        sys.exit( "The file %s does not exist. Continue plotting without eigen data." \
                  % eigendatafile )


    max_num_steps = 1000

    # plot the x-axis
    plt.plot( [0, max_num_steps], [0, 0], 'black' )

    # go through the file and plot the eigenvalues line by line
    num_steps = 0
    for line in file_content:
        items = line.split()
        if items[0] == '#': continue
        data = [ float(item) for item in items ]
        # there are four opening columns before the eigenvalues are listed
        num_eigenvals = len( data ) - 4
        num_steps = max( num_steps, data[0] )
        plt.plot( np.array( [data[0]] ).repeat( num_eigenvals), 
                  data[4:],
                  'xk' )

    # set the bounds
    plt.xlim( 0, num_steps )
#    plt.ylim( -2.5, 0.5 )
    
    # set title
    plt.xlabel("Continuation step")


    if tikzfile:
        matplotlib2tikz.save( tikzfile )
    else:
        plt.show()

    return
# ------------------------------------------------------------------------------
if __name__ == "__main__":
    _main()
# ------------------------------------------------------------------------------
