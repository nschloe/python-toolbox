#! /usr/bin/env python
# -*- coding: utf-8 -*-
# ==============================================================================
import numpy as np
from math import pi, sin, cos, sqrt
from cmath import exp, phase
from string import zfill
import sys, subprocess, os, shutil, xml.dom.minidom

import vtkio, symmetries
# ==============================================================================
'''Default task file for regular continuation.'''
DEFAULT_CONTINUATION_FILE = os.path.join( os.path.dirname( os.path.realpath(__file__) ),
                                          "default-continuation.xml" )

DEFAULT_TURNINGPOINT_FILE = os.path.join( os.path.dirname( os.path.realpath(__file__) ),
                                          "default-tp.xml" )

'''Default task file for branch hopping.'''
DEFAULT_BRANCHJUMP_FILE   = os.path.join( os.path.dirname( os.path.realpath(__file__) ),
                                          "default-hopper.xml" )
               
''' Width of the numeric indexing in files, e.g., "005" in solution005.vti.'''
INDEX_WIDTH = 3

'''Default file name for initial states on branches.'''
DEFAULT_INIT_STATE_FILENAME = "init.vti"

'''Running index of total branches.'''
BRANCH_INDEX = 0

'''Running index of failed branch switches.'''
FAIL_INDEX = 0

'''Directory where the branch switch takes place.'''
BRANCH_SWITCH_DIR = "./switch"
BRANCH_SWITCH_INDEX = 0

BISECT_DIR = "./bisect"
BISECT_INDEX = 0

TP_DIR = "./tp"
'''Index for the turning point continuations.'''
TP_INDEX = 0

'''Path to the piro-driver workhorse.'''
PIRO_DRIVER_EXE = "/home/nico/gl-trilinos/build/serial/executables/piro-driver/piro-driver.out"
# ==============================================================================
def _get_xml_attribute( xml_file, attribute ):

    dom = xml.dom.minidom.parse( xml_file )
                           
    # reset the predictor to predictor_file
    for item in dom.getElementsByTagName("Parameter"):
        if item.getAttribute("name") == attribute:
            return item.getAttribute( "value" )
    
    return
# ==============================================================================
def _create_directory( directory ):

    if not os.path.exists( directory ):
        os.makedirs( directory )
    elif not os.path.isdir( directory ):
        # exists but is not folder: delete and create
        os.remove( directory )
        os.mkdir( directory )

    return
# ==============================================================================
def _set_xml_attribute( xml_file, attribute, value ):

    dom = xml.dom.minidom.parse( xml_file )
                           
    # reset the predictor to predictor_file
    for item in dom.getElementsByTagName("Parameter"):
        if item.getAttribute("name") == attribute:
            item.setAttribute( "value", value )
            
    # write the contents back to the file and close
    file_handle = open( xml_file, "w" )
    
    # Remove the first line of the XML representation;
    # Teuchos can't deal with it.
    content = dom.documentElement.toprettyxml( indent="", newl="" )
    file_handle.write( content )
    file_handle.close()
    
    return
# ==============================================================================
def _run_piro( xml_file ):
    """
    Runs a given command on the command line and returns its output.
    """
    #print command

    command = PIRO_DRIVER_EXE + " --xml-input-file=%s" % xml_file

    out_file = "piro.out"
    err_file = "piro.err"

    # strore stdout and stderr in files
    directory = os.path.dirname( xml_file )
    out_file = os.path.join( directory, out_file )
    err_file = os.path.join( directory, err_file )
    f_out = open( out_file, "w" )
    f_err = open( err_file, "w" )

    process = subprocess.Popen( command,
                                shell = True,
                                stdout = f_out,
                                stderr = f_err,
                                close_fds = True )
    #output = process.stdout.read()[:-1]
    ret = process.wait()
    
    # close the log files
    f_out.close()
    f_err.close()

    if ret != 0:
        fence = 80 * "="
        print "\nERROR: The command \n\n%s\n%s\n%s\n\nreturned a nonzero " \
              "exit status \"%d\". The error message is stored in file \"%s\"." % \
              ( fence, command, fence, ret, err_file )
              

    return out_file, err_file
# ==============================================================================
def _create_new_branch_folder( solution_file, is_forward_step ):

    global BRANCH_INDEX

    # create the branch folder
    branch_folder = "branch%d" % BRANCH_INDEX
    if not os.path.exists( branch_folder ):
        os.mkdir( branch_folder )
        os.mkdir( os.path.join( branch_folder, 'data') )
    elif not os.path.isdir( branch_folder ):
        # exists but is not folder: delete and create
        os.remove( branch_folder )
        os.mkdir( branch_folder )
        os.mkdir( os.path.join( branch_folder, 'data') )

    init_state_path = os.path.join( branch_folder, DEFAULT_INIT_STATE_FILENAME )
    
    print "    Found new branch! Storing in the folder \"%s\"." % branch_folder

    shutil.copyfile( solution_file, init_state_path )

    xml_path = os.path.join( branch_folder, "conf.xml" )
    shutil.copyfile( DEFAULT_CONTINUATION_FILE, xml_path )
    
    # set the initial step direction in the new file
    step_size = abs( float( _get_xml_attribute( xml_path, "Initial Step Size" )
                          )
                   )
    if is_forward_step:
        _set_xml_attribute( xml_path, "Initial Step Size", str(step_size) )
    else:
        _set_xml_attribute( xml_path, "Initial Step Size", str(-step_size) )

    BRANCH_INDEX += 1

    return xml_path, init_state_path
# ==============================================================================
def _branch_switch_nullstate( base_state, nullstate ):

    global BRANCH_SWITCH_INDEX    
    this_branch_switch_dir = os.path.join( BRANCH_SWITCH_DIR, "switch%d" % \
                                           BRANCH_SWITCH_INDEX )
    BRANCH_SWITCH_INDEX += 1
    _create_directory( this_branch_switch_dir )
    switcher_xml_path = os.path.join ( this_branch_switch_dir, "conf.xml" )
    shutil.copyfile( DEFAULT_BRANCHJUMP_FILE, switcher_xml_path )
    
    # Copy over base state and nullstate, and make sure their filenames are
    # allright in the task file.
    base_state_path = os.path.join ( this_branch_switch_dir,
                                     os.path.basename(base_state) )
    shutil.copyfile( base_state, base_state_path )
    
    nullstate_path = os.path.join ( this_branch_switch_dir,
                                    os.path.basename(nullstate) )
    shutil.copyfile( nullstate, nullstate_path )

    _set_xml_attribute( switcher_xml_path,
                        "State", os.path.basename(base_state) )
    _set_xml_attribute( switcher_xml_path,
                        "Predictor", os.path.basename(nullstate) )
    
    # get the parameter value
    state_psi, bounds, h, field_data = vtkio.reader( base_state )
    h0 = field_data[ "H0" ]
    
    # To compare the initial states later, do not limit the number of steps
    # to one brach-switch step or similar, but rather aim for a limit in
    # the parameter value. This makes sure that all the switched states
    # have the same parameter value, so it's easier to compare them to tell
    # whether they constitute separate branches or not.
    eps = 1.0e-2
    _set_xml_attribute( switcher_xml_path,
                        "Max Value", str(h0 + eps) )
    _set_xml_attribute( switcher_xml_path,
                        "Min Value", str(h0 - eps) )
    
    print "Branch switch with one nullspace vector..."
    # run the branch switcher
    _run_piro( switcher_xml_path )
    print "done."
    
    new_xml_files = []
    
    print "Trying to branch switch... ",
    
    # file name of the file that is produced if everything goes all right
    import glob
    sol_filename = sorted( glob.glob( os.path.join(
                                  this_branch_switch_dir, "solution*.vti" ) )
                          )[-1]
    
    if ( os.path.isfile(sol_filename) ): # successful _run
    
        print "success!"
        psi0, bounds, h, field_data = vtkio.reader( sol_filename )
        new_h0 = field_data[ "H0" ]
        xml_path, init_state_path = _create_new_branch_folder( sol_filename,
                                                               new_h0 > h0 )
        new_xml_files.append( switcher_xml_path )
        
    else:
        global FAIL_INDEX
        fail_file = "fail%d.out" % FAIL_INDEX
        print "failed! The output is stored in file \"%s\"." % fail_file
        f = open( fail_file, 'w' )
        f.write( output )
        f.close()
        FAIL_INDEX += 1
          
    return new_xml_files
# ==============================================================================
def _branch_switch_two_nullstates( base_state, nullstate0, nullstate1 ):

    # get the parameter value
    state_psi, bounds, h, field_data = vtkio.reader( base_state )
    h0 = field_data[ "H0" ]
    # --------------------------------------------------------------------------
    # read the files
    null0_psi, bounds, h, field_data = vtkio.reader( nullstate0 )
    null1_psi, bounds, h, field_data = vtkio.reader( nullstate1 )
    # --------------------------------------------------------------------------
    # list of local branches
    local_branches_startpoints = []
    # Divide [0:2*pi] into N uniform sections and loop over the intermediate
    # points. Disregard the last value; it's 2*pi and thus equivalent to 0.
    N = 8
    new_xml_files = []
    for theta in np.linspace( 0, 2*pi, num=N+1 )[0:-1]:
      
        global BRANCH_SWITCH_INDEX    
        this_branch_switch_dir = os.path.join( BRANCH_SWITCH_DIR, "switch%d" % \
                                               BRANCH_SWITCH_INDEX )
        BRANCH_SWITCH_INDEX += 1
        _create_directory( this_branch_switch_dir )
        switcher_xml_path = os.path.join ( this_branch_switch_dir, "conf.xml" )
        shutil.copyfile( DEFAULT_BRANCHJUMP_FILE, switcher_xml_path )
      
        # move the base state file over
        shutil.copyfile( base_state,
                         os.path.join ( this_branch_switch_dir, "state.vti" )
                       )
        _set_xml_attribute( switcher_xml_path,
                            "State", "state.vti" )
        # similar for the predictor
        predictor_filepath = os.path.join( this_branch_switch_dir,
                                           "newPredictor.vti" )
        _set_xml_attribute( switcher_xml_path,
                            "Predictor", "newPredictor.vti" )
                         
        # To compare the initial states later, do not limit the number of steps
        # to one brach-switch step or similar, but rather aim for a limit in
        # the parameter value. This makes sure that all the switched states
        # have the same parameter value, so it's easier to compare them to tell
        # whether they constitute separate branches or not.
        eps = 1.0e-2
        _set_xml_attribute( switcher_xml_path,
                            "Max Value", str(h0 + eps) )
        _set_xml_attribute( switcher_xml_path,
                            "Min Value", str(h0 - eps) )

        print "\nTry next eigenvalue combination:"
        print str(cos(theta)) + " * null0  +  " + \
              str(sin(theta)) + " * null1 ..."
        
        # form linear combination of the null vectors
        psi_new = cos(theta) * null0_psi \
                + sin(theta) * null1_psi

        # write the predictor file
        vtkio.writer( predictor_filepath, psi_new, bounds )
        
        print "Trying to branch switch... ",
        outfile, errfile = _run_piro( switcher_xml_path )
        
        # The final solution is the last of the solution*vti files.
        import glob
        sol_filename = sorted( glob.glob( os.path.join(
                                     this_branch_switch_dir, "solution*.vti" ) )
                             )[-1]
        
        if ( os.path.isfile(sol_filename) ): # successful _run
            print "success!"

            print "    Matching new branch against already found branches..."
            psi0, bounds, h, field_data = vtkio.reader( sol_filename )
        
            # check if this orbit has been found already
            is_new_branch = True
            symmetry_tol = 1.0e-5
            for local_branch_startpoint in local_branches_startpoints:
                psi1, bounds, h, field_data = vtkio.reader( local_branch_startpoint )
                if symmetries.check_group_orbit( psi0, psi1, bounds, h, symmetry_tol ):
                    is_new_branch = False
                    print "    Detected branch sits in the same group orbit as \"" + local_branch_startpoint + "\". Skip." 
                    break
            
            if is_new_branch:
                H0 = field_data[ "H0" ]
                psi0, bounds, h, field_data = vtkio.reader( base_state )
                orig_H0 = field_data[ "H0" ]
                xml_path, init_state_path = \
                                      _create_new_branch_folder( sol_filename,
                                                                 H0 > orig_H0 )
                local_branches_startpoints.append( init_state_path )
                new_xml_files.append( xml_path )

        else:
            global FAIL_INDEX
            fail_file_out = os.path.join( this_branch_switch_dir,
                                          "fail%d.out" % FAIL_INDEX )
            fail_file_err = os.path.join( this_branch_switch_dir,
                                          "fail%d.err" % FAIL_INDEX )
            print "failed! The output is stored in the files \"%s\" and \"%s\"." \
                  % ( fail_file_out, fail_file_err )
            
            os.rename( os.path.join( this_branch_switch_dir, "piro.out" ),
                       fail_file_out )
            os.rename( os.path.join( this_branch_switch_dir, "piro.err" ),
                       fail_file_err )
            FAIL_INDEX += 1
            
    return new_xml_files
# ==============================================================================
def _zero_in( original_base_state, original_null_state ):
  '''Zeroes in onto a sates with corresponding null states by
        1.) Determine the base state plus all nullstates somewhat accurately
            by successively decreasing the step size starting from the step
            before the branch, and
        2.) Given a reasonably good approximation, use turning point
            continuation to zoom in accurately. This step has to be done
            for each null state separately.
      The reason why step 1 is necessary at all is that turning point
      continuation can be very sensitive to initial guesses, so it will be useful
      to start off with very good guess.'''

  # do bisection to get base and null vector states
  parameter_step_direction = 1
  print " > _zero_in ", original_base_state
  approximate_base_state, approximate_null_states = \
      _bisect_zoom( original_base_state, parameter_step_direction )

  # successively zoom in on the states using turning point continuation
  accurate_base_state = []
  accurate_nullvector_states = []
  for approximate_null_state in approximate_null_states:
      accurate_base_state, tmp = \
          _turning_point_zoom( original_base_state, original_null_state )
      accurate_nullvector_states.append( tmp )

  return accurate_base_state, accurate_nullvector_states
# ==============================================================================
def _turning_point_zoom( original_base_state, original_null_state ):
    ''' Uses Piro's turning point continuation to zoom in into a given
        base state/null state pair.
        Returns the filenames of the accurate solution and null vector.'''
    
    global TP_INDEX
    directory = os.path.join( TP_DIR, "tp%d" % TP_INDEX )
    _create_directory( directory )
    TP_INDEX += 1
    
    # copy over the XML file
    tp_xml_file = os.path.join( directory,
                                os.path.basename(DEFAULT_TURNINGPOINT_FILE) )
    
    shutil.copyfile( DEFAULT_TURNINGPOINT_FILE, tp_xml_file )
    
    # Copy over the states -- not strictly necessary,
    # but useful for post-run checks.
    base_state_basename = os.path.basename( original_base_state )
    base_state_path = os.path.join( directory, base_state_basename )
    shutil.copyfile( os.path.realpath(original_base_state),
                     base_state_path )
                     
    null_state_basename = os.path.basename( original_null_state )
    null_state_path = os.path.join( directory, null_state_basename )
    shutil.copyfile( os.path.realpath(original_null_state),
                     null_state_path )
    
    # set the input file names in the turning point continuation
    _set_xml_attribute( tp_xml_file,
                        "State", base_state_basename )
    _set_xml_attribute( tp_xml_file,
                        "Null state", null_state_basename )
    
    print "Doing one turning point continuation step " \
          "to zero in on a branch point/nullvector combination..."
    # run piro
    outfile, errfile = _run_piro( tp_xml_file )
    
    # check the output
    accurate_solution_state   = os.path.join( directory,
                                              "solution0-state.vti" )
    accurate_nullvector_state = os.path.join( directory,
                                              "solution0-nullvector.vti" )
    
    if not os.path.isfile( accurate_solution_state ):
        print "fail!"
        print "Turning point continuation unsuccessful, " \
              "solution state file \"%s\" not found. std::out and std::err " \
              "can be checked in \"%s\" and \"%s\", respectively." % \
              ( accurate_solution_state, outfile, errfile )
        return None, None
    elif not os.path.isfile( accurate_nullvector_state ):
        print "fail!"
        print "Turning point continuation unsuccessful. " \
              "Solution nullvector file \"%s\" not found. std::out and " \
              "std::err can be checked in \"%s\" and \"%s\", respectively." % \
              ( accurate_solution_state, outfile, errfile )
        return None, None
    else:
        print "done."
    
    return accurate_solution_state, accurate_nullvector_state
# ==============================================================================
def _bisect_zoom( original_base_state, parameter_step_direction ):

    # positive or negative parameter continuation direction
    parameter_step_direction = +1;

    global BISECT_INDEX

    initial_step_size = 1.0e-3
    minimum_step_size = 1.0e-10
    alpha = 0.5
    
    print " > original_base_state ", original_base_state
    new_base_state = original_base_state
    
    # --------------------------------------------------------------------------
    while initial_step_size > minimum_step_size:

        directory = os.path.join( BISECT_DIR, "bisect%d" % BISECT_INDEX )
        _create_directory( directory )
        BISECT_INDEX += 1
        
        # create the base folder
        _create_directory( os.path.join( directory, "data" ) )
        
        # copy over the XML file
        continuation_xml_file = os.path.join( directory,
                                              os.path.basename(DEFAULT_CONTINUATION_FILE) )
        
        shutil.copyfile( DEFAULT_CONTINUATION_FILE, continuation_xml_file )
                        
        # Copy over the base state.
        print " > new_base_state ", new_base_state
        base_state_basename = os.path.basename( new_base_state )
        base_state_path = os.path.join( directory, base_state_basename )
        shutil.copyfile( os.path.realpath(original_base_state),
                         base_state_path )
        # ----------------------------------------------------------------------
        # Adjust the XML attributes.
        # set the input file names in the turning point continuation
        _set_xml_attribute( continuation_xml_file,
                            "State", base_state_basename )

        _set_xml_attribute( continuation_xml_file,
                            "Initial Step Size", str(initial_step_size) )

        # Don't be too aggressive here either.
        # TODO Use constant step size? Set maximum step size?
        _set_xml_attribute( continuation_xml_file,
                            "Aggressiveness", str(1.0) )
        # ----------------------------------------------------------------------
        # run piro
        print "Doing a continuation run to zoom in on a branch point " \
              "(initial step size = %s)..." % str(initial_step_size)
        outfile, errfile = _run_piro( continuation_xml_file )
        # ----------------------------------------------------------------------
        # check the output
        new_base_state = 1
        
        
        #accurate_solution_state   = os.path.join( directory,
                                                  #"solution0-state.vti" )
        #accurate_nullvector_state = os.path.join( directory,
                                                  #"solution0-nullvector.vti" )
        
        #if not os.path.isfile( accurate_solution_state ):
            #print "fail!"
            #print "Turning point continuation unsuccessful, " \
                  #"solution state file \"%s\" not found. std::out and std::err " \
                  #"can be checked in \"%s\" and \"%s\", respectively." % \
                  #( accurate_solution_state, outfile, errfile )
            #return None, None
        #elif not os.path.isfile( accurate_nullvector_state ):
            #print "fail!"
            #print "Turning point continuation unsuccessful. " \
                  #"Solution nullvector file \"%s\" not found. std::out and " \
                  #"std::err can be checked in \"%s\" and \"%s\", respectively." % \
                  #( accurate_solution_state, outfile, errfile )
            #return None, None
        #else:
            #print "done."
        # ----------------------------------------------------------------------
        
        # change the initial step size for the next run
        initial_step_size *= alpha
    # --------------------------------------------------------------------------
    # retrieve the approximate solution and nullstates
    
    # --------------------------------------------------------------------------
    return approximate_solution_state, approximate_null_states
# ==============================================================================
def _l2_norm( psi, h ):
    """L^2(\Omega)-norm of a given state psi."""
    alpha = h.prod() * ( psi.conjugate() * psi ).sum().real
    return sqrt( alpha )
# ==============================================================================
def _match_states( reference_states, states ):
    '''Matches a set of states 'states0' with another set of states 'states1'
       and returns the states in 'states1' which do not already
       (approximately) appear in 'states0'.'''
       
    # This is a very loose tolerance -- should be alright to uniquely
    # identify the eigenstates though.
    tol = 1.0e-2
    
    new_states = []
    
    for statefile in states:
        # read the states file
        psi1, bounds, spacing, field_data = vtkio.reader( statefile )
        
        has_appeared = False
        for reference_state in reference_states:
            ref_psi, bounds, h, field_data = vtkio.reader( reference_state )

            alpha = exp( 1j* (  phase( ref_psi[0,0] )
                               -phase( psi1[0,0] ) 
                             )
                       )
            diff = ref_psi - alpha * psi1
            norm = _l2_norm( diff, h )
            print norm
            if norm < tol:
                has_appeared = True
                break
            
        if not has_appeared:
            new_states.append( statefile )
    
    return new_states
# ==============================================================================
def _prepare_branch_switch( directory, num_new_unstable_states, k ):
    '''Prepare branch switch folder.'''

    # What was the base state before the change in stability again?
    base_state_basename = "solution" + \
                          zfill( str(k), INDEX_WIDTH )
    base_state = os.path.join( directory, "data", base_state_basename + ".vti" )

    # Bisect to get as close to get a close approximation for the branch
    # point.
    # TODO fix 
    parameter_step_direction = +1
    print " > _prepare_branch_switch", base_state, parameter_step_direction
    approximate_solution_state, approximate_nullvector_states = \
        _bisect_zoom( base_state, parameter_step_direction )
  
    accurate_base_state, accurate_null_states = _zero_in( base_state )

    # gather the previously unstable states
    previous_ueigenstates = []
    base_state_basename = "solution" + zfill( str(k), INDEX_WIDTH )
    for i in range(num_ueigenvalues[k]):
        previous_ueigenstates.append( os.path.join( directory, "data",
                                                    base_state_basename +
                                                    "-ueigenstate" + str( i ) + ".vti"
                                                  )
                                    )
    # gather the current unstable states
    current_ueigenstates = []
    base_state_basename = "solution" + zfill( str(k+1), INDEX_WIDTH )
    for i in range( num_ueigenvalues[k+1] ):
        current_ueigenstates.append( os.path.join( directory, "data",
                                                  base_state_basename +
                                                    "-ueigenstate" + str( i ) + ".vti"
                                                 )
                                   )

    if num_new_unstable_states > 0:
        # solution went more unstable
        branch_switch_index = k + 1            
        nullstates = _match_states( previous_ueigenstates,
                                    current_ueigenstates )
    elif num_new_unstable_states < 0:
        # solution went more unstable
        branch_switch_index = k
        nullstates = _match_states( current_ueigenstates,
                                    previous_ueigenstates )
    else:
        sys.exit( "Change in stability was expected." )

    # zero in on all states
    accurate_nullstates = []
    for k in range( len(nullstates) ):
        accurate_states = _zero_in( base_state, nullstates[k] )
        if accurate_states[0] is not None:
            accurate_base_state = accurate_states[0]
            accurate_nullstates.append( accurate_states[1] )

    # create new branch task files
    if len(accurate_nullstates) == 0:
        print "Don't create a new branch hop; no data available."
    elif len(accurate_nullstates) == 1:
        new_xml_files += _branch_switch_nullstate( accurate_base_state,
                                                    accurate_nullstates[0] )
    elif len(accurate_nullstates) == 2:
        new_xml_files += _branch_switch_two_nullstates( accurate_base_state,
                                                        accurate_nullstates[0],
                                                        accurate_nullstates[1] )
    else:
        sys.exit( "The nullstate is of dimension %d.\n"
                  "Don't know how to deal with this yet." % len(accurate_nullstates) )
                  
    return new_xml_files
# ==============================================================================
def _branch_explorer( xml_file ):
  
    directory = os.path.dirname( xml_file )
    _create_directory( os.path.join( directory, "data" ) )
  
    # do the continuation
    print "Performing continuation in \"%s\"..." % os.path.dirname( xml_file )
    _run_piro( xml_file )
    print "done."
    
    # read eigendata file
    eigendata_file = os.path.join( directory,
                                   "eigenvalues.dat" )
    index_unstable_eigenvalues_column = 1
    try:
        # read as strings b/c some entries may be '---------'
        eigen_data = np.loadtxt( eigendata_file,
                                 dtype = str )
        if len(eigen_data.shape) == 1: # only one row
            num_ueigenvalues = np.array( [ int(eigen_data[index_unstable_eigenvalues_column ]) ] )
        else:
            num_ueigenvalues = np.array( [ int(entry) for entry in \
                               eigen_data[:, index_unstable_eigenvalues_column ] ] )
    except IOError:
        sys.exit( "The file \"%s\" does not exist. Abort." % eigendata_file )
        
    if len( num_ueigenvalues ) == 1:
       # Just made one step into the new branch -- strange.
       print "WW Made no step in the branch. Check your log files."
       return 0
      
    diff = num_ueigenvalues[1:] - num_ueigenvalues[0:-1]
    # Where diff is nonzero, the number of unstable eigenvalues has changed.
    # Fetch those.
    nullstates = []
    new_xml_files = []
    
    min_step_size = 1.0e-10
    
    for k in range(len(diff)):
        if diff[k] != 0:
            # prepare branch switch folder
            new_xml_files = _prepare_branch_switch( os.path.dirname( xml_file ),
                                                    diff[k],
                                                    k
                                                  )

    # recursively explore the branch
    for new_xml_file in new_xml_files:
        _branch_explorer( new_xml_file )
    
    return 0
# ==============================================================================
if __name__ == '__main__':

    #base_state = 'state.vti'
    #nullstate0 = 'nullstate0.vti'
    #nullstate1 = 'nullstate1.vti'

    #_branch_switch_two_nullstates( base_state, nullstate0, nullstate1 )
    
    INITIAL_STATE = 'init.vti'
    XML_PATH, BRANCH0_INIT = _create_new_branch_folder( INITIAL_STATE,
                                                        is_forward_step = True
                                                      )
    
    # start the recursive call
    _branch_explorer( XML_PATH )
# ==============================================================================