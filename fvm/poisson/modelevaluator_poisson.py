#! /usr/bin/env python
# -*- coding: utf-8 -*-
'''
Solve Poisson's equation with finite volumes.
'''
import vtk
import vtkio
import numpy as np
from scipy import sparse
import math, cmath
# #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=
class ginla_model_evaluator:
    '''
    Ginzburg--Landau model evaluator class.
    '''
    # ==========================================================================
    def __init__( self, mesh, mu ):
        '''
        Initialization. Set mesh.
        '''
        self.mesh = mesh
        self.laplacian = None
        self.control_volumes = None
    # ==========================================================================
    def compute_jacobian( self, current_psi, psi ):
        '''
        Laplacian operator
        '''
        if self.laplacian is None:
            self._assemble_laplacian()

        return self.laplacian * psi - self.control_volumes * psi
    # ==========================================================================
    def _assemble_laplacian( self ):
        '''
        Create FVM equation system for Poisson's problem with Dirichlet boundary
        conditions.
        '''
        num_nodes = len( self.mesh.nodes )

        self.laplacian = sparse.lil_matrix(  ( num_nodes, num_nodes )  )

        # compute the FVM entities for the mesh
        edge_lengths, coedge_edge_ratios = self._create_fvm_entities()

        k = 0
        for element in self.mesh.elements:
            # loop over the edges
            l = 0
            for edge in element.edges:
                # --------------------------------------------------------------
                # Compute the integral
                #
                #    I = \int_{x0}^{xj} (xj-x0).A(x) dx
                #
                # numerically by the midpoint rule, i.e.,
                #
                #    I ~ |xj-x0| * (xj-x0) . A( 0.5*(xj+x0) ).
                #
                node0 = self.mesh.nodes[ edge[0] ].coords
                node1 = self.mesh.nodes[ edge[1] ].coords
                midpoint = 0.5 * ( node0 + node1 )

                a_project = np.dot( node1 - node0,
                                    self._magnetic_vector_potential( midpoint )
                                  )

                # one-point approximation
                # of the integral of a.edge over the edge
                a_integral = a_project * edge_lengths[k][l]

                # sum it into the matrix
                self.laplacian[ edge[0], edge[0] ] += coedge_edge_ratios[k][l]
                self.laplacian[ edge[0], edge[1] ] -= coedge_edge_ratios[k][l] 
                self.laplacian[ edge[1], edge[0] ] -= coedge_edge_ratios[k][l]
                self.laplacian[ edge[1], edge[1] ] += coedge_edge_ratios[k][l]
                l += 1
            k += 1

        # transform the matrix into the more efficient CSR format
        self.laplacian = self.laplacian.tocsr()

        return
    # ==========================================================================
    def _create_fvm_entities( self ):
        '''
        Computes the area of the control volumes
        and the lengths of the edges and co-edges.
        '''
        # ----------------------------------------------------------------------
        def _triangle_circumcenter( x0, x1, x2 ):
            '''
            Compute the circumcenter of a triangle.
            '''
            w = np.cross( x0-x1, x1-x2 )
            omega = 2.0 * np.dot( w, w )

            if abs(omega) < 1.0e-10:
                raise ZeroDivisionError( "Division by 0." )

            alpha = np.dot( x1-x2, x1-x2 ) * np.dot( x0-x1, x0-x2 ) / omega
            beta  = np.dot( x2-x0, x2-x0 ) * np.dot( x1-x2, x1-x0 ) / omega
            gamma = np.dot( x0-x1, x0-x1 ) * np.dot( x2-x0, x2-x1 ) / omega

            return alpha * x0 + beta * x1 + gamma * x2
        # ----------------------------------------------------------------------
        def _triangle_area( x0, x1, x2 ):
            '''
            Compute the area of a triangle.
            '''
            return 0.5 * np.linalg.norm( np.cross( x1-x0, x2-x0 ) )
        # ----------------------------------------------------------------------

        num_nodes = len( self.mesh.nodes )
        self.control_volumes = np.zeros( num_nodes )

        edge_lengths = []
        coedge_edge_ratios = []

        for element in self.mesh.elements:
            if element.element_type != vtk.VTK_TRIANGLE:
                print "Control volumes can only be constructed consistently " \
                      "with triangular elements."
                raise NameError

            # compute the circumcenter of the element
            x0 = self.mesh.nodes[ element.nodes[0] ].coords
            x1 = self.mesh.nodes[ element.nodes[1] ].coords
            x2 = self.mesh.nodes[ element.nodes[2] ].coords
            cc = _triangle_circumcenter( x0, x1, x2 )

            edge_length = []
            coedge_edge_ratio = []
            for edge in element.edges:
                x0 = self.mesh.nodes[ edge[0] ].coords
                x1 = self.mesh.nodes[ edge[1] ].coords
                midpoint = 0.5 * ( x0 + x1 )
                # control volume contributions
                self.control_volumes[ edge[0] ] += _triangle_area( x0,
                                                                   cc,
                                                                   midpoint
                                                                 )
                self.control_volumes[ edge[1] ] += _triangle_area( x1,
                                                                   cc,
                                                                   midpoint
                                                                 )

                edge_length.append( np.linalg.norm( x1-x0 ) )
                coedge_edge_ratio.append(   np.linalg.norm( midpoint - cc )
                                          / np.linalg.norm( x1       - x0 )
                                        )

            edge_lengths.append( edge_length )
            coedge_edge_ratios.append( coedge_edge_ratio )

        return edge_lengths, coedge_edge_ratios
    # ==========================================================================
# #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=