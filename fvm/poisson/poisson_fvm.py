#! /usr/bin/env python
# -*- coding: utf-8 -*-
'''
Solve Poisson's equation with finite volumes.
'''
import vtk
import vtkio
import numpy as np
from scipy import sparse
from scipy.sparse.linalg import cg, LinearOperator, dsolve
from scipy.sparse.linalg import spilu # only supported from scipy 0.8.0 onwards
from scipy.sparse import triu, tril
import sys
import math
import matplotlib.pyplot as pp
#from numpy import linalg
#from numpy.random import rand
#import time
# ==============================================================================
def _main():
    '''
    Main function.
    '''
    # --------------------------------------------------------------------------
    def _scaled_l2norm( x ):
        return math.sqrt( np.dot( control_volumes * x, x ) )
    # --------------------------------------------------------------------------
    def _add_residual( xk ):
        '''
        Callback function for the linear iteration.
        '''
        residuals.append( _scaled_l2norm( matrix*xk - rhs ) )
        return
    # --------------------------------------------------------------------------
    def _jacobi_preconditioner( x ):
        '''
        Returns the solution of Jacobi aka diagonal preconditioning.
        '''
        return x / matrix.diagonal()
    # --------------------------------------------------------------------------
    def _ilu_preconditioner( x ):
        '''
        Returns the solution of Jacobi aka diagonal preconditioning.
        '''
        return spilu( matrix ).solve( x )
    # --------------------------------------------------------------------------

    opts, args = _parse_input_arguments()

    # read the mesh
    print "Reading the mesh..."
    try:
        mesh = vtkio.read_mesh( opts.filename )
    except AttributeError:
        print "Could not read from file ", opts.filename, "."
        sys.exit()
    print " done."

    #print mesh.elements[0]
    #print mesh.nodes[0].is_on_boundary
    
    # compute the FVM entities for the mesh
    print "Creating FVM entities..."
    control_volumes, coedge_edge_ratios = _create_fvm_entities( mesh )
    print " done."

    # prepare the equation system
    print "Creating equation system..."
    matrix, rhs = _create_equation_system( mesh,
                                           control_volumes,
                                           coedge_edge_ratios
                                         )
    print " done."

    # initial guess
    x0 = np.zeros( len(rhs) )

    ## solve the system
    #x = linalg.dsolve(A, rhs)

    # --------------------------------------------------------------------------
    #print "Solving the system without preconditioning..."
    #_add_residual( x0 )
    #solution, info = cg( MATRIX, RHS,
                         #x0 = x0,
                         #tol = 1.0e-10,
                         #maxiter = None,
                         #xtype = None,
                         #M = None,
                         #callback = _add_residual
                       #)
    #print " done."

    #assert( info == 0 )

    ## compute relative residuals
    #global RESIDUALS
    #num_steps = len( RESIDUALS )
    #relative_residuals0 = np.zeros( num_steps )
    #for k in range( num_steps ):
        #relative_residuals0[k] = RESIDUALS[k] / _scaled_l2norm( MATRIX*x0 - RHS )
    # --------------------------------------------------------------------------
    print "Solving the system with ILU preconditioning..."
    #RESIDUALS = []
    # create the linear operator object
    prec = LinearOperator( (len(rhs), len(rhs)),
                           matvec = _ilu_preconditioner
                         )
    residuals = []
    _add_residual( x0 )
    solution, info = cg( matrix, rhs,
                         x0 = x0,
                         tol = 1.0e-10,
                         maxiter = 300,
                         xtype = None,
                         M = prec,
                         callback = _add_residual
                       )
    print " done."

    print info
    #assert( info == 0 )

    # compute relative residuals
    num_steps = len( residuals )
    relative_residuals1 = np.zeros( num_steps )
    for k in range( num_steps ):
        relative_residuals1[k] = residuals[k] #/ _scaled_l2norm( matrix*x0 - rhs )
    # --------------------------------------------------------------------------
    #print RESIDUALS
    #print (
    #pp.semilogy( relative_residuals0 )
    pp.semilogy( relative_residuals1 )
    pp.show()
    #print RESIDUALS / _scaled_l2norm( MATRIX*x0 - RHS )

    # write the solution to a file
    sol_filename = "solution.vtu"
    vtkio.write_mesh( sol_filename, mesh, solution )
    
    return
# ==============================================================================
def _parse_input_arguments():
    '''
    Parse input arguments.
    '''
    from optparse import OptionParser

    parser = OptionParser()
    parser.add_option( "-f", "--file",
                       dest = "filename",
                       type = str,
                       help = "read mesh from VTKFILE",
                       metavar = "VTKFILE"
                     )
    #parser.add_option("-q", "--quiet",
                      #action="store_false", dest="verbose", default=True,
                      #help="don't print status messages to stdout")

    (opts, args) = parser.parse_args()
    return opts, args
# ==============================================================================
def _create_equation_system( mesh, control_volumes, coedge_edge_ratios ):
    '''
    Create FVM equation system for Poisson's problem with Dirichlet boundary
    conditions.
    '''
    num_nodes = len( mesh.nodes )

    A = sparse.lil_matrix(  ( num_nodes, num_nodes )  )

    k = 0
    for element in mesh.elements:
        # loop over the edges
        l = 0
        for edge in element.edges:
            # sum the stuff into the matrix
            #values = [ - coedge_edge_ratios[k][l],
                         #coedge_edge_ratios[k][l] ]

            A[ edge[0], edge[0] ] += coedge_edge_ratios[k][l]
            A[ edge[0], edge[1] ] -= coedge_edge_ratios[k][l]
            A[ edge[1], edge[0] ] -= coedge_edge_ratios[k][l]
            A[ edge[1], edge[1] ] += coedge_edge_ratios[k][l]
            l += 1
        k += 1

    # create right hand side
    f = 1.0
    rhs = np.empty( num_nodes )
    for k in range( num_nodes ):
        # this is the integration rule -- midpoint
        rhs[ k ] = f * control_volumes[ k ]

    # Adjust the equation system for Dirichlet boundary.
    # TODO This piece of code is sloooow..
    for k in range( num_nodes ):
        if mesh.nodes[k].is_on_boundary:
            A[ k, : ] = 0.0
            A[ k, k ] = 1.0
            rhs[ k ]  = 0.0

    # transform A into the more efficient CSR format
    A = A.tocsr()

    return A, rhs
# ==============================================================================
def _create_fvm_entities( mesh ):
    '''
    Computes the area of the control volumes and the lengths of the edges and
    co-edges.
    '''
    # --------------------------------------------------------------------------
    def _triangle_circumcenter( x0, x1, x2 ):
        '''
        Compute the circumcenter of a triangle.
        '''
        w = np.cross( x0-x1, x1-x2 )
        omega = 2.0 * np.dot( w, w )

        if abs(omega) < 1.0e-10:
            print "Division by 0."
            raise ZeroDivisionError

        alpha = np.dot( x1-x2, x1-x2 ) * np.dot( x0-x1, x0-x2 ) / omega
        beta  = np.dot( x2-x0, x2-x0 ) * np.dot( x1-x2, x1-x0 ) / omega
        gamma = np.dot( x0-x1, x0-x1 ) * np.dot( x2-x0, x2-x1 ) / omega

        return alpha * x0 + beta * x1 + gamma * x2
    # --------------------------------------------------------------------------
    def _triangle_area( x0, x1, x2 ):
        '''
        Compute the area of a triangle.
        '''
        return 0.5 * np.linalg.norm( np.cross( x1-x0, x2-x0 ) )
    # --------------------------------------------------------------------------
    
    num_nodes = len( mesh.nodes )
    control_volumes = np.zeros( num_nodes )

    coedge_edge_ratios = []

    for element in mesh.elements:
        if element.element_type != vtk.VTK_TRIANGLE:
            print "Control volumes can only be constructed consistently " \
                  "with triangular elements."
            raise NameError

        # compute the circumcenter of the element
        x0 = mesh.nodes[ element.nodes[0] ].coords
        x1 = mesh.nodes[ element.nodes[1] ].coords
        x2 = mesh.nodes[ element.nodes[2] ].coords
        cc = _triangle_circumcenter( x0, x1, x2 )
    
        coedge_edge_ratio = []
        for edge in element.edges:
            x0 = mesh.nodes[ edge[0] ].coords
            x1 = mesh.nodes[ edge[1] ].coords
            midpoint = 0.5 * ( x0 + x1 )
            # control volume contributions
            control_volumes[ edge[0] ] += _triangle_area( x0, cc, midpoint )
            control_volumes[ edge[1] ] += _triangle_area( x1, cc, midpoint )

            # coedge-edge ratios
            coedge_edge_ratio.append(   np.linalg.norm( midpoint - cc )
                                      / np.linalg.norm( x1       - x0 )
                                    )

        coedge_edge_ratios.append( coedge_edge_ratio )

    return control_volumes, coedge_edge_ratios
# ==============================================================================
if __name__ == "__main__":
    _main()

    #import cProfile
    #cProfile.run( '_main()', 'pfvm_profile.dat' )
# ==============================================================================