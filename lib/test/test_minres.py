#!/usr/bin/env python
# ==============================================================================
import numerical_methods as nm
from scipy.sparse import spdiags
import numpy as np
# ==============================================================================
# construct matrix
n = 10
m = 1 # number of negative eigenvalues

data = np.array( range( 1, 2*n, 2 ) ) - 2 * m
A = spdiags( data, 0, n, n )

# construct lhs and rhs
x0 = np.zeros( n )
b = np.ones( n )

x, info, relresvec = nm.minres_wrap( A, x0, b )

print relresvec
print info
