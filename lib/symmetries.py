#! /usr/bin/env python
# -*- coding: utf-8 -*-
# ==============================================================================
"""
Check which symmetries are present in a state given by a VT{I,K} file.
"""
# ==============================================================================
import numpy as np
from cmath import exp, pi, phase
from math import sqrt

INDEX = 0
# ==============================================================================
def identity( psi ):
    return psi
# ==============================================================================
def rot90( psi ):
    return np.rot90( psi )
# ==============================================================================
def rot180( psi ):
    return np.rot90( np.rot90(psi) )
# ==============================================================================
def rot270( psi ):
    return np.rot90( rot180(psi) )
# ==============================================================================
def conjugated_fliplr( psi ):
    return np.fliplr(psi).conjugate()
# ==============================================================================
def fliplr( psi ):
    return np.fliplr(psi)
# ==============================================================================
def conjugated_flipud( psi  ):
    return flipud(psi).conjugate()
# ==============================================================================
def flipud( psi ):
    return np.flipud(psi)
# ==============================================================================
def diagonal1( psi ):
    return np.rot90( fliplr(psi).conjugate() )
# ==============================================================================
def diagonal2( psi ):
    return np.rot90( flipud(psi).conjugate() )
# ==============================================================================
def l2_norm( psi, h ):
    """L^2(\Omega)-norm of a given state psi."""
    alpha = h.prod() * ( psi.conjugate() * psi ).sum().real
    return sqrt( alpha )
# ==============================================================================
SYMMETRY_TRANSFORMATIONS = [ identity,
                             rot90,
                             rot180,
                             rot270,
                             conjugated_fliplr,
                             fliplr,
                             conjugated_flipud,
                             flipud,
                             diagonal1,
                             diagonal2
                           ]
# ==============================================================================
def check_group_orbit( psi0, psi1, bounds, h, tol = 1.0e-10 ):
    """Check if the two states psi0 and psi1 sit in one group orbit."""

    # Loop over all possible transformations and check if you get the original
    # psi0 back.
    # The exact value of the complex angle does not matter; hence align them
    # first in point [0,0].
    for transformation in SYMMETRY_TRANSFORMATIONS:
        psi1_transformed = transformation(psi1)
        alpha = exp( 1j* (  phase( psi0[0,0] )
                           -phase( psi1_transformed[0,0] ) 
                         )
                   )
        psi1_transformed =  alpha * psi1_transformed
        diff =  psi0 - psi1_transformed
        
        #import vtkio
        #global INDEX
        #vtkio.writer( "diff%d.vti" % INDEX, diff, bounds )
        norm = l2_norm( diff, h )
        #print INDEX, norm
        #INDEX += 1
        
        if norm < tol:
            return True
            
    return False
# ==============================================================================
if __name__ == '__main__':
    sys.exit( 1 )
# ==============================================================================