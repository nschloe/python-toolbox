#! /usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib as mpl

from cmath import phase, pi

# ==============================================================================
def plot_parameter_space( continuation_data, num_unstable_eigenvalues, plot_columns ):

    # don't start from 0: compatibility with eigen_data!
    x_values = continuation_data[0:,plot_columns[0]]
    y_values = continuation_data[0:,plot_columns[1]]

    if num_unstable_eigenvalues is None or len(num_unstable_eigenvalues) == 0:
        # just plain plot the graph with no indication about stability
        mpl.pyplot.plot( x_values, y_values, 'r-' )
    else:
        n = len(num_unstable_eigenvalues)
        # It may be that the eigenvalue hasn't been  computed yet.
        # Hence, possibly disregard the last entry.
        stable_points   = np.ma.masked_where( num_unstable_eigenvalues > 0,
                                              y_values[0:n] )
        unstable_points = np.ma.masked_where( num_unstable_eigenvalues == 0,
                                              y_values[0:n] )

        mpl.pyplot.plot( x_values[0:n], stable_points,   'r-',
                         x_values[0:n], unstable_points, 'r--' )

    mpl.pyplot.ylim( -1.0, 0.0 )

    return
# ==============================================================================
def plot_solution( psi, bounds, fig, subplot0=121, subplot1=122 ):
    fig.add_subplot(subplot0)
    im0 = plot_psi_op( psi, bounds )

    fig.add_subplot(subplot1)
    im1 = plot_psi_arg( psi, bounds )

    return [im0, im1]
# ==============================================================================
# plot order parameter
def plot_psi_op( psi, bounds ):

    # The image bounds are differ from the "physical" bounds
    # by exactly h/2 on either side.
    dims = psi.shape
    hx = (bounds[1]-bounds[0]) / (dims[0]-1)
    hy = (bounds[3]-bounds[2]) / (dims[1]-1)
    image_bounds = [ bounds[0]-hx/2, bounds[1]+hx/2,
                     bounds[2]-hy/2, bounds[3]+hy/2 ]

    im = mpl.pyplot.imshow( abs(psi)**2,
                            origin = 'upper',
                            cmap   = mpl.cm.gray,
                            vmin   = 0.0,
                            vmax   = 1.0,
                            interpolation = "Nearest",
                            extent = image_bounds )

    mpl.pyplot.xticks( bounds[0:2], ("%0.1f" % bounds[0], "%0.1f" % bounds[1]) )
    mpl.pyplot.yticks( bounds[2:4], ("%0.1f" % bounds[2], "%0.1f" % bounds[3]) )
    mpl.pyplot.title("$||\psi||^2$")
    #mpl.pyplot.colorbar( ticks=(0, 0.5, 1) )

    mpl.pyplot.xlim( bounds[0], bounds[1] )
    mpl.pyplot.ylim( bounds[2], bounds[3] )

    return im
# ==============================================================================
# plot argument
def plot_psi_arg( psi, bounds ):

    dims = psi.shape

    # The image bounds are differ from the "physical" bounds
    # by exactly h/2 on either side.
    shape = psi.shape
    hx = (bounds[1]-bounds[0]) / (dims[0]-1)
    hy = (bounds[3]-bounds[2]) / (dims[1]-1)
    image_bounds = [ bounds[0]-hx/2, bounds[1]+hx/2,
                     bounds[2]-hy/2, bounds[3]+hy/2 ]

    psi_arg  = np.ndarray(shape=dims, dtype=float)
    for i in range(dims[0]):
        for j in range(dims[1]):
            psi_arg[i, j] = phase( psi[i, j] )

    im = mpl.pyplot.imshow( psi_arg,
                            origin = 'upper',
                            cmap = mpl.cm.hsv,
                            vmin = -pi,
                            vmax =  pi,
                            interpolation = "Nearest",
                            extent = image_bounds )

    mpl.pyplot.xticks( bounds[0:2], ("%0.1f" % bounds[0], "%0.1f" % bounds[1]) )
    mpl.pyplot.yticks( bounds[2:4], ("%0.1f" % bounds[2], "%0.1f" % bounds[3]) )
    mpl.pyplot.title("$\mathrm{arg}\,\psi$")
    #cc = mpl.pyplot.colorbar(ticks=(-pi, 0, pi))
    #cc.ax.set_yticklabels( ['$-\pi$', '0', '$\pi$'])

    mpl.pyplot.xlim( bounds[0], bounds[1] )
    mpl.pyplot.ylim( bounds[2], bounds[3] )

    return im
# ==============================================================================
def mark_vortices( psi, bounds ):
    from scipy.ndimage import filters

    shape = psi.shape
    hx = (bounds[1]-bounds[0]) / (shape[0]-1)
    hy = (bounds[3]-bounds[2]) / (shape[1]-1)

    # find minima
    a = abs(psi)**2
    b = filters.minimum_filter( a, size=3 )
    I, J = np.where( a==b )

    for (i, j) in zip(I, J):
        # count the vorticity
        v = count_vorticity( psi, bounds, (i, j) )
        if v > 0:
            text = "+" + str(v)
        else:
            text = str(v)

        # Get a better guess of the approximate position of the vortex.
        # Get the +-2 environment and pass it to the least squares fit
        # translate into coordinates.
        indices_x = np.array( range( i-2, i+2+1 ) )
        indices_y = np.array( range( j-2, j+2+1 ) )
        X = bounds[0] + indices_x * hx
        Y = bounds[3] - indices_y * hy
        data = abs( psi[ i-2:i+2+1, j-2:j+2+1 ] )**2
        xpos, ypos = linear_least_squares_fit( X, Y, data )

        mpl.pyplot.annotate( text, (xpos, ypos),
                             horizontalalignment = "center",
                             verticalalignment = "center",
                             color = "white" )

    return
# ==============================================================================
# Fit a function
#
#    (X-A)^2 = x^2 + y^2 - 2*alpha*x - 2*beta*y + alpha^2 + beta^2
#
# to a given set of x, y values with data.
def nonlinear_least_squares_fit( x, y, data ):
    # create the equation system
    #k = 0
    #A = np.zeros( (x.size*y.size,4) )
    #b = np.zeros( (x.size*y.size,1) )
    #for i in range(0,x.size):
        #for j in range(0,y.size):
            #A[k,0] = x[i]**2
            #A[k,1] = y[j]**2
            #A[k,2] = x[i]*y[j]
            #A[k,3] = 1
            #b[k] = data[i,j]
            #k = k + 1

    #print A
    #print b

    #(coeffs, residuals, rank, sing_vals) = np.linalg.lstsq( A, b )
    #print coeffs
    #print "gg"

    # calculate the extreme point of the polynomial with the given coefficients
    #print


    # prepare the arrays
    X = np.zeros( (x.size*y.size, 2) )
    Z = np.zeros( (x.size*y.size, 1) )
    k = 0
    for i in range(0, x.size):
        for j in range(0, y.size):
            X[k, 0] = x[i]
            X[k, 1] = y[j]
            Z[k] = data[i, j]
            k = k + 1

    # TODO give intial guess p0=
    from scipy.optimize import curve_fit
    popt, pcov = curve_fit( model_function, X, Z )


    #return
# ==============================================================================
def model_function( x, alpha, beta ):
    return (x[0]-alpha)**2 + (x[1]-beta)**2
# ==============================================================================
# Fit a function
#
#    alpha*(x-x0)^2 + beta*(y-y0)^2 + delta
# =   alpha*x^2 - alpha*2*x*x0 + alpha*x0^2
#   + beta* y^2 - beta* 2*y*y0 + beta* y0^2
#                                + delta
# =: a0*x^2 + a1*y^2 - 2*a2*x - 2*a3*y + a4
#
# to a given set of x, y values with data, and return the values
# x0, y0.
def linear_least_squares_fit( x, y, data ):
    # create the equation system
    k = 0
    A = np.zeros( (x.size*y.size, 5) )
    b = np.zeros( (x.size*y.size, 1) )
    for i in range(0, x.size):
        for j in range(0, y.size):
            A[k, 0] = x[i]**2
            A[k, 1] = y[j]**2
            A[k, 2] = -2*x[i]
            A[k, 3] = -2*y[j]
            A[k, 4] = 1
            b[k] = data[i, j]
            k = k + 1

    # solve by linear least squares
    (coeffs, residuals, rank, sing_vals) = np.linalg.lstsq( A, b )

    alpha = coeffs[0]
    beta  = coeffs[1]
    x0    = coeffs[2] / alpha
    y0    = coeffs[3] / beta

    #print "delta = ", coeffs[4] - alpha*x0**2 - beta*y0**2

    return x0, y0
# ==============================================================================
# counts the vorticity of psi around any given point
def count_vorticity( psi, bounds, (i,j) ):

    # distance of the edges of the pixel box around the pixel
    dist = 2

    # go around the boundary of a square 5x5 box centered around (i,j)
    # and gather the PSI values;
    # first gather upper and lower limits for i,j (boundaries!)
    if  i-dist < 0:
        lower_i = 0
        upper_i = min( lower_i + 2*dist, psi.shape[0]-1 )
    elif i+dist > psi.shape[0]-1:
        upper_i = psi.shape[0]-1
        lower_i = max( upper_i - 2*dist , 0 )
    else:
        lower_i = i - dist
        upper_i = i + dist

    if  j-dist < 0:
        lower_j = 0
        upper_j = min( lower_j + 2*dist, psi.shape[1]-1 )
    elif j+dist > psi.shape[1]-1:
        upper_j = psi.shape[1]-1
        lower_j = max( upper_j - 2*dist , 0 )
    else:
        lower_j = j - dist
        upper_j = j + dist

    vals = ( psi[range(lower_i, upper_i)    , lower_j                     ],
             psi[upper_i                    , range(lower_j, upper_j)     ],
             psi[range(upper_i, lower_i, -1), upper_j                     ],
             psi[lower_i                    , range(upper_j, lower_j, -1) ]
           )

    vals = np.concatenate( vals )

    # return the winding number
    return winding_number( vals.real, vals.imag )
# ==============================================================================
def plot_complex_sequence( Z ):

    mpl.pyplot.plot( Z.real, Z.imag, 'ro-' )

    mpl.pyplot.xlim( min(Z.real), max(Z.real) )
    mpl.pyplot.ylim( min(Z.imag), max(Z.imag) )

    mpl.pyplot.show()

    return
# ==============================================================================
# determines the winding number of a closes polygon w.r.t. the origin
def winding_number( X, Y ):
    wnum = 0

    tol = 1.0e-15

    n = len(X)

    for k in range(0, n):
        k_current = k
        if k == n-1:
            k_next = 0
        else:
            k_next = k+1

        is_k0_neg = Y[k_current] < 0.0
        is_k1_neg = Y[k_next]    < 0.0

        if is_k0_neg ^ is_k1_neg: # crosses real axes
            # calculate the intersection point
            r = X[k_current] + Y[k_current] * ( X[k_next] - X[k_current] ) / \
                                              ( Y[k_current] - Y[k_next] )
            if r < 0.0:
                if is_k0_neg: # crossing clockwise
                    wnum -= 1
                else: # crossing counter-clockwise
                    wnum += 1
        elif abs( Y[k_current] ) < tol and X[k_current] < 0.0:
            if is_k1_neg: # crossing clockwise
                wnum += 0.5
            else: # crossing counter-clockwise
                wnum -= 0.5
        elif abs( Y[k_next] ) < tol and X[k_next] < 0.0:
            if is_k0_neg: # crossing clockwise
                wnum -= 0.5
            else: # crossing counter-clockwise
                wnum += 0.5

    return wnum
# ==============================================================================
